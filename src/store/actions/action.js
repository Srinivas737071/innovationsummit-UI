/* eslint-disable no-extra-bind */
import { push } from "react-router-redux";
import ConfigSettings from "../../appConfig";
import axios from "axios";
import { Modal, Button } from 'antd';
import * as moment from 'moment';
import 'moment/locale/pt-br';

const { confirm } = Modal;

export const ADMINLOGIN = "ADMINLOGIN";
export const GETUSERDATA = "GETUSERDATA";
export const GET_CONTACT_USER_DATA = "GET_CONTACT_USER_DATA";
export const DELETEUSER = "DELETEUSER";
export const UPDATEUSER = "UPDATEUSER";
export const GETHOTELDATA = "GETHOTELDATA";
export const SEARCHUSER = "SEARCHUSER";
export const CLEARUSER = "CLEARUSER";
export const CREATE_HOTEL = "CREATE_HOTEL";
export const GETINNOVATIONDATA = "GETINNOVATIONDATA";
export const CREATE_INNOVATION = "CREATE_INNOVATION";
export const UPDATE_INNOVATION = "UPDATE_INNOVATION";
export const VIEW_INNOVATION_IMAGE = "VIEW_INNOVATION_IMAGE";
export const GETINNOVATIONLOCATION = "GETINNOVATIONLOCATION";
export const VIEW_INNOVATION_USERS = "VIEW_INNOVATION_USERS";
export const VIEW_INNOVATION_SPEARK = "VIEW_INNOVATION_SPEARK";
export const VIEW_INNOVATION_HELPDESK = "VIEW_INNOVATION_HELPDESK";
export const USER_TRAVEL_DETAILS = "USER_TRAVEL_DETAILS";
export const USER_ACCOMMODATION = "USER_ACCOMMODATION";
export const USER_ACCOMMODATION_DETAILS = "USER_ACCOMMODATION_DETAILS";
export const GET_CONCLAVE_DATA = "GET_CONCLAVE_DATA";
export const AGENDA_DETAILS = "AGENDA_DETAILS";
export const GALLERY_DETAILS = "GALLERY_DETAILS";
export const ITENERY_DETAILS = "ITENERY_DETAILS";
export const SEARCH_REGISTATION_USER = "SEARCH_REGISTATION_USER";
export const CLEAR_REGISTRATION_USER = "CLEAR_REGISTRATION_USER";
export const KIOSK_USER_LIST = "KIOSK_USER_LIST";
export const KIOSK_ALERT_LIST = "KIOSK_ALERT_LIST";


export const login = values => {
  return async dispatch => {
    return await axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.users + '?type=admin&email=' + values.email + '&password=' + values.password )
      .then(response => {
        if(response.data.length > 0) {
          localStorage.setItem('loginUser', JSON.stringify(response.data));
          dispatch(push("/user"));
        }
        else if (response.data.length === 0 ) {
          alert('error while logging in');
        }
      })
  };
};

export const getUserData = () => {
  return async dispatch => {
    return await axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.users)
      .then(response =>{
        dispatch({
          type: GETUSERDATA,
          data: response.data
        })
    });
  };
};

export const getContactPersonData = () => {
  return async dispatch => {
    return await axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.users)
      .then(response =>{
        dispatch({
          type: GET_CONTACT_USER_DATA,
          data: response.data
        })
      });
  };
};

export const createUserData = request => {
  return async dispatch => {
    return await axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.users, request)
      .then(result => {
        if(request.pictures) {
          let data= new FormData();
          data.append('files', request.pictures[0].originFileObj);
          data.append('field', 'picture');
          data.append('ref', 'person');
          data.append('refId', result.data._id);
         dispatch(uploadImage(data,'user'));
        }
        else if(!request.pictures){
          setTimeout(
            function() {
              dispatch(getUserData());
            // eslint-disable-next-line no-extra-bind
            }.bind(this),
            0
          );
        }
    });
  };
}

export const uploadImage = (request,typeValue) => {
  return async dispatch => {
    return await axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.uploadImage, request)
      .then(result => {
        setTimeout(
          function() {
            if(typeValue === 'user') {
              dispatch(getUserData());
            }
            else if(typeValue === 'innovation') {
              dispatch(getInnovationData());
            }
            else if(typeValue === 'gallery') {
              dispatch(getGalleryDetails());
            }
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
    });
  };
}

export const updateUserData = (request,editId) => {
  return async dispatch => {
    return await axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.users + '/'+ editId, request)
      .then(result => {
        if(request.pictures) {
          let data= new FormData();
          data.append('files', request.pictures[0].originFileObj);
          data.append('field', 'picture');
          data.append('ref', 'person');
          data.append('refId', result.data._id);
          dispatch(uploadImage(data,'user'));
        }
        else if(!request.pictures){
          setTimeout(
            function() {
              dispatch(getUserData());
            // eslint-disable-next-line no-extra-bind
            }.bind(this),
            0
          );
        }
      });
  };
};

export const deleteUserData = (key) => {
  return async dispatch => {
    return await axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.users + '/'+ key)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getUserData());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
}

export const searchUser = (val, data) => {
  if (val) {
    let filter = {
      location: val,
      type: val,
      company: val,
      email: val,
      name: val
    };
    let filterKeys1 = Object.keys(filter);
    let searchreult = data.filter(item => {
      return filterKeys1.some(keyName => {
        return (
          new RegExp(filter[keyName], "gi").test(item[keyName]) ||
          filter[keyName] === ""
        );
      });
    });
    return {
      type: SEARCHUSER,
      data: searchreult
    };
  }
  else if(!val){
    return {
      type: CLEARUSER
    };
  }
};

export const getInnovationData = () => {
  return dispatch => {
    return axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.innovations)
      .then(response => {
        dispatch({
          type: GETINNOVATIONDATA,
          data: response.data
        });
    });
  };
};

export const getInnovationDetails = id => {
  return dispatch => {
    return axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.innovations + '?slug=' +id)
      .then(response => {
        dispatch({
          type: GET_CONCLAVE_DATA,
          data: response.data
        });
      });
  };
};

export const messageNotification = (data,selectedType) => {
  return dispatch => {
    return axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.messages, data)
      .then(response => {
        setTimeout(
          function() {
            dispatch(searchRegistartion(null,selectedType));
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
    });
  };
}

export const addregistrations = (request,selectedType) => {
  console.log(request, "req");
  return dispatch => {
    return axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.registrations, request)
      .then(response => {
        let data={}
        if(request.note === "accommodation") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {startDate: request.dateTime,endDate: request.checkOutDateTime,lookupName: request.lookupName,headerTitle: request.headerTitle}
          }
        }
        else if (request.note === "travel") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {
              startDate: request.dateTime,
              endDate: request.checkOutDateTime,
              from:request.From,
              to:request.To,
              images:request.Image,
              travelType:request.travelType,
              lookupName: request.lookupName,
              cabdriverName: request.cabdriverName,
              cabNumber: request.cabNumber,
              cabContactNumber: request.cabContactNumber,
              travelStatus: request.travelStatus,
              headerTitle: request.headerTitle,
              isConnectingFlight: request.isConnectingFlight,
              connectingFlight: request.connectingFlight
            }
          }
        }
        else if (request.note === "itenery") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {
              lookupName: request.lookupName,
              agenda:request.agenda,
              dateTime:request.dateTime,
              headerTitle: request.headerTitle
            }
          }
        }
          dispatch(messageNotification(data,selectedType));
      });
  };
}

export const editMessage = (request, id,selectedType) => {
  return dispatch => {
    return axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.messages + '/' + id, request)
      .then(response => {
         setTimeout(
          function() {
            dispatch(searchRegistartion(null,selectedType));
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
}

export const editregistrations = (request, registrationid,messageid,selectedType) => {
  return dispatch => {
    return axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.registrations + '/' + registrationid, request)
      .then(response => {
        let data={}
        if(request.note === "accommodation") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {startDate: request.dateTime,endDate: request.checkOutDateTime,lookupName: request.lookupName,headerTitle: request.headerTitle}
          }
        }
        else if (request.note === "travel") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {
              startDate: request.dateTime,
              endDate: request.checkOutDateTime,
              from:request.From,
              to:request.To,
              images:request.Image,
              travelType:request.travelType,
              lookupName: request.lookupName,
              cabdriverName: request.cabdriverName,
              cabNumber: request.cabNumber,
              cabContactNumber: request.cabContactNumber,
              travelStatus: request.travelStatus,
              headerTitle: request.headerTitle,
              isConnectingFlight: request.isConnectingFlight,
              connectingFlight: request.connectingFlight
            }
          }
        }
        else if (request.note === "itenery") {
          data= {
            registration:response.data.id,
            title: request.note,
            content: {
              lookupName: request.lookupName,
              agenda:request.agenda,
              dateTime:request.dateTime,
              headerTitle: request.headerTitle
            }
          }
        }
        dispatch(editMessage(data,messageid,selectedType));
      });
  };
}

export const deleteregistrations = (registrationid,selectedType) => {
  return dispatch => {
    return axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.registrations + '/' + registrationid)
      .then(response => {
         setTimeout(
          function() {
            dispatch(searchRegistartion(null,selectedType));
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
}

export const deleteMessage = (messageid,registrationid,selectedType) => {
  return dispatch => {
    return axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.messages + '/' + messageid)
      .then(response => {
         setTimeout(
          function() {
            dispatch(deleteregistrations(registrationid,selectedType));
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
}
export const getAgendaDetails = (id) => {
  let data = {"operationName":"session","variables":{"where":{"conclave":{"slug":id}}},"query":"query session($where: JSON, $sort: String){\n  sessions(where: $where, sort: $sort) {\n    id\n    title\n    _id\n    description\n    startDateTime\n    endDateTime\n    conclave {\n      type\n      _id\n      slug\n      name\n      startDate\n      description\n      createdAt\n      updatedAt\n      contactPersons {\n        id\n        name\n        email\n        primaryContactCountryCode\n        primaryContactNo\n        company\n        designation\n      }\n      galleries {\n        id\n        media {\n          url\n        }\n        description\n        dateTime\n        _id\n      }\n      location {\n        _id\n        line1\n        city\n        country\n        state\n        zipcode\n        locationMapUrl\n        updatedAt\n        createdAt\n        id\n      }\n      banner {\n        id\n        url\n      }\n      logo {\n        id\n        url\n      }\n    }\n    speakers {\n      type\n      _id\n      name\n      email\n      primaryContactCountryCode\n      summary\n      slug\n      location\n      designation\n      password\n      description\n      company\n      team\n      primaryContactNo\n      createdAt\n      updatedAt\n      picture {\n        url\n        id\n        _id\n      }\n    }\n  }\n}\n"};
  return dispatch => {
    return axios
      .post(ConfigSettings.graphUrl, data)
      .then(response => {
        dispatch({
          type: AGENDA_DETAILS,
          data: response.data.data.sessions
        });
    });
  };
}

export const getItenery = (id,email) => {
  let data = {"operationName":"message","variables":{"where":{"title":"itenery","registration":{"conclave":{"slug": id},"persons":{"email":email}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
   return dispatch => {
    return axios
      .post(ConfigSettings.graphUrl, data)
      .then(response => {
        dispatch({
          type: ITENERY_DETAILS,
          data: response.data.data.messages
        });
    });
  };
}

export const addAgenda = (request) => {
  return dispatch => {
    return axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.agenda, request)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getAgendaDetails());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
    });
  }
}

export const updateAgenda = (request,id) => {
  return dispatch => {
    return axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.agenda + '/' + id, request)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getAgendaDetails());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
    });
  }
}

export const deleteAgenda = (id) => {
  return dispatch => {
    return axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.agenda + '/' + id)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getAgendaDetails());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
    });
  }
}

export const addInnovation = request => {
  return dispatch => {
    return axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.innovations, request)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getInnovationData());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
};

export const updateInnovation = (request, id) => {
  return dispatch => {
    return axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.innovations + '/' + id, request)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getInnovationData());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
};

export const deleteInnovation = (key) => {
  return async dispatch => {
    return await axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.innovations + '/'+ key)
      .then(result => {
        setTimeout(
          function() {
            dispatch(getInnovationData());
          // eslint-disable-next-line no-extra-bind
          }.bind(this),
          0
        );
      });
  };
}

export const getHelpdeskInnovationDeatils = request => {
  let data =   {"operationName":"conclave","variables":{"where":{"slug":request.Id}},"query":"query conclave($where: JSON, $sort: String) {\n  conclaves(where: $where, sort: $sort){\n    type\n    _id\n    slug\n    name\n    startDate\n    description\n    createdAt\n    updatedAt\n    endDate\n    contactPersons {\n      id\n      name\n      email\n      primaryContactCountryCode\n      primaryContactNo\n      company\n      designation\n    location\n}\n  }\n}\n"};
    return dispatch => {
    return axios
      .post(
        ConfigSettings.graphUrl ,
        data
      )
      .then(response => {
        dispatch({
          type: VIEW_INNOVATION_HELPDESK,
          data: response.data.data.conclaves[0].contactPersons
        });
      });
  };
};

export const getregistration = request => {
    let data = {"operationName":"message","variables":{"where":{"registration":{"conclave":{"slug": request}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
        return dispatch => {
        return axios
          .post(
            ConfigSettings.graphUrl,
            data
          )
          .then(response => {
              dispatch({
                  type: USER_ACCOMMODATION_DETAILS,
                  data: response.data.data.messages
              })
          });
    };
}


 export const getTravelDetails = request => {
   let data = { EventId: request };
  return dispatch => {
      return axios
        .post(
          ConfigSettings.baseUrl + ConfigSettings.getUserTravelDeatils,
          data
        )
        .then(response => {
          dispatch({
              type: USER_TRAVEL_DETAILS,
              data: response.data.data
          })
        });
  };
}

export const addTravelDetails = request => {
  let url = window.location.href;
  let urlParams = url.split("?id=");
  let eventid = atob(urlParams[1]);
  return dispatch => {
    return axios
      .post(
        ConfigSettings.baseUrl + ConfigSettings.addUserTravelDetails,
        request
      )
      .then(response => {
        setTimeout(
            function() {
            dispatch(getTravelDetails(eventid));
            // eslint-disable-next-line no-extra-bind
            }.bind(this),
            0
        );
      });
  };
}
//addTravelDetails

export const updateTravelDetails = request => {
  let url = window.location.href;
  let urlParams = url.split("?id=");
  let eventid = atob(urlParams[1]);
  return dispatch => {
      return axios
        .post(
          ConfigSettings.baseUrl + ConfigSettings.updateUserTravelDetails,
          request
        )
        .then(response => {
          setTimeout(
              function() {
                dispatch(getTravelDetails(eventid));
              // eslint-disable-next-line no-extra-bind
              }.bind(this),
              0
          );
      });
  };
}

export const deleteTravelDetails = id => {
  let url = window.location.href;
  let urlParams = url.split("?id=");
  let eventid = atob(urlParams[1]);
  return dispatch => {
      return axios
        .post(
          ConfigSettings.baseUrl + ConfigSettings.deleteUserTravelDetails,
          id
        )
        .then(response => {
          setTimeout(
              function() {
                dispatch(getTravelDetails(eventid));
              // eslint-disable-next-line no-extra-bind
              }.bind(this),
              0
          );
      });
  };
}

export const getGalleryDetails = () => {
  return dispatch => {
    return axios
      .get(ConfigSettings.swaggerUrl + ConfigSettings.galleryDetails)
      .then(response => {
        dispatch({
            type: GALLERY_DETAILS,
            data: response.data
        })
    });
  };
}

export const addGallery = (data) => {
  return dispatch => {
    return axios
      .post(ConfigSettings.swaggerUrl + ConfigSettings.galleryDetails, data)
      .then(response => {
        setTimeout(
          function() {
            dispatch(getGalleryDetails());
          }.bind(this),
          0
        );
    });
  };
}

export const editGallery = (id, data) => {
  return dispatch => {
    return axios
      .put(ConfigSettings.swaggerUrl + ConfigSettings.galleryDetails + '/' + id, data)
      .then(response => {
        setTimeout(
          function() {
            dispatch(getGalleryDetails());
          }.bind(this),
          0
        );
    });
  };
}

export const deleteGallery = (id) => {
  return dispatch => {
    return axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.galleryDetails + '/' + id)
      .then(response => {
        setTimeout(
          function() {
            dispatch(getGalleryDetails());
          }.bind(this),
          0
        );
    });
  };
}

export const deleteImage = (id) => {
  return dispatch => {
    return axios
      .delete(ConfigSettings.swaggerUrl + ConfigSettings.deleteImage + '/' + id)
      .then(response => {
        setTimeout(
          function() {
            dispatch(getGalleryDetails());
          }.bind(this),
          0
        );
    });
  };
}

export const sentEmailItenery = request => {
  return dispatch => {
    return axios
      .post(ConfigSettings.baseUrl + ConfigSettings.sentEmailItenery, request)
      .then(result => {
        
      });
  };
};

export const searchRegistartion = (email,title) => {
 let data = '';
 let url = window.location.href;
  let urlParams = url.split("?id=");
  let slug = atob(urlParams[1]);
  if(email && slug && title){
   data = {"operationName":"message","variables":{"where":{"title":title,"registration":{"conclave":{"slug": slug},"persons":{"email":email}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
  }
  if(!email && slug && title){
    data = {"operationName":"message","variables":{"where":{"title":title,"registration":{"conclave":{"slug": slug}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
   }
   if(email && slug && !title){
    data = {"operationName":"message","variables":{"where":{"registration":{"conclave":{"slug": slug},"persons":{"email":email}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
   }
   if(!email && slug && !title){
    data = {"operationName":"message","variables":{"where":{"registration":{"conclave":{"slug": slug}}}},"query":"query message($where: JSON, $sort: String){\n  messages(where: $where, sort: $sort) {\n    id\n    _id\n    messageLogTime\n    title\n    reminder\n    note\n    reminderTime\n    content\n    createdAt\n    updatedAt\n    registration {\n      _id\n      note\n      lookupName\n      dateTime\n      updatedAt\n      createdAt\n      id\n      checkOutDateTime\n      conclave {\n        type\n        _id\n        slug\n        name\n        startDate\n        description\n        createdAt\n        updatedAt\n        contactPersons {\n          id\n          name\n          email\n          primaryContactCountryCode\n          primaryContactNo\n          company\n          designation\n        }\n        galleries {\n          id\n          media {\n            url\n          }\n          description\n          dateTime\n          _id\n        }\n        location {\n          _id\n          line1\n          city\n          country\n          state\n          zipcode\n          locationMapUrl\n          updatedAt\n          createdAt\n          id\n        }\n        banner {\n          id\n          url\n        }\n        logo {\n          id\n          url\n        }\n      }\n      persons {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n      registeredBy {\n        type\n        _id\n        name\n        email\n        primaryContactCountryCode\n        summary\n        slug\n        location\n        designation\n        password\n        description\n        company\n        team\n        primaryContactNo\n        createdAt\n        updatedAt\n        picture {\n          url\n          id\n          _id\n        }\n      }\n    }\n  }\n}\n"};
   }     
  return dispatch => {
        return axios
          .post(
            ConfigSettings.graphUrl,
            data
          )
          .then(response => {
              dispatch({
                  type: USER_ACCOMMODATION_DETAILS,
                  data: response.data.data.messages
              })
          });
    }; 
}

export const kioskList = (slug) => {
    let data = {"operationName":"message","variables":{"where":{"title":"travel","registration":{"conclave":{"slug": slug}}}},"query":"query message($where: JSON, $sort: String){\n messages(where: $where, sort: $sort) {\n id\n _id\n messageLogTime\n title\n reminder\n note\n reminderTime\n content\n createdAt\n updatedAt\n registration {\n _id\n note\n lookupName\n dateTime\n updatedAt\n createdAt\n id\n checkOutDateTime\n conclave {\n type\n _id\n slug\n name\n startDate\n description\n createdAt\n updatedAt\n contactPersons {\n id\n name\n email\n primaryContactCountryCode\n primaryContactNo\n company\n designation\n }\n galleries {\n id\n media {\n url\n }\n description\n dateTime\n _id\n }\n location {\n _id\n line1\n city\n country\n state\n zipcode\n locationMapUrl\n updatedAt\n createdAt\n id\n }\n banner {\n id\n url\n }\n logo {\n id\n url\n }\n }\n persons {\n type\n _id\n name\n email\n primaryContactCountryCode\n summary\n slug\n location\n designation\n password\n description\n company\n team\n primaryContactNo\n createdAt\n updatedAt\n picture {\n url\n id\n _id\n }\n }\n registeredBy {\n type\n _id\n name\n email\n primaryContactCountryCode\n summary\n slug\n location\n designation\n password\n description\n company\n team\n primaryContactNo\n createdAt\n updatedAt\n picture {\n url\n id\n _id\n }\n }\n }\n }\n}\n"};
    return dispatch => {
      return axios
        .post(ConfigSettings.graphUrl,data)
        .then(response => {
        let resultData = response.data.data.messages;
        let filterData = resultData.filter((o) => { return o.content.lookupName==="aeroplane"});
        let x;
        if (filterData.length > 0) {
          x = filterData.filter(o => {
            return o.content.travelStatus === "scheduled";
          });
          x.sort(function(a, b) {
            var c = new Date(a.content.endDate);
            var d = new Date(b.content.endDate);
            return c - d;
          });
        }
        console.log(x, "sorted");
          dispatch({
          type: KIOSK_USER_LIST,
          data: filterData,
          alertList: x
          })
        });
    };
  }

  // export const kioskAlerts = (kioskUser) => {
  //   let x;
  //   if (kioskUser.length > 0) {
  //     x = kioskUser.filter(o => {
  //       return o.content.travelStatus === "scheduled";
  //     });
  //     x.sort(function(a, b) {
  //       var c = new Date(a.content.endDate);
  //       var d = new Date(b.content.endDate);
  //       return c - d;
  //     });
  //   }
  //   console.log(x, "sorted");
  //   return dispatch => {
  //     console.log(x,"inside");
  //     dispatch({
  //       type: KIOSK_ALERT_LIST,
  //       data: x
  //       }) 
  //   }
  // }
  // export const KioskSearch = (value) => {

  // }
  
