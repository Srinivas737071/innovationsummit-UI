import { GETHOTELDATA } from '../actions/action';

const initialState = {
    data: []
}

const hotel  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case GETHOTELDATA:   
            return {
                ...state,
                data: action.data
            }    
    }
    return state;
}

export default hotel;