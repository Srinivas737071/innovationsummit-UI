import { KIOSK_USER_LIST, KIOSK_ALERT_LIST } from '../actions/action';

const initialState = {
    kioskUserlist: [],
    kioskAlertlist: []
}

const kiosk  = (state = initialState, action) => {
    switch(action.type) {
        case KIOSK_USER_LIST:   
            return {
                ...state,
                kioskUserlist: action.data,
                kioskAlertlist: action.alertList
            }
    }
    return state;
}

export default kiosk;
