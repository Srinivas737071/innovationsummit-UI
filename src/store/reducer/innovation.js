import { 
    GETINNOVATIONDATA, 
    GET_CONTACT_USER_DATA,
    VIEW_INNOVATION_IMAGE, 
    GETINNOVATIONLOCATION,
    VIEW_INNOVATION_USERS,
    VIEW_INNOVATION_SPEARK,
    VIEW_INNOVATION_HELPDESK,
    GET_CONCLAVE_DATA,
    AGENDA_DETAILS,
    GALLERY_DETAILS
 } from '../actions/action';

const initialState = {
    data: [],
    contactPerson: [],
    conclave: {},
    imageData:[],
    hotelLocation: [],
    innovationUsers: [],
    speake:[],
    helpDesk:[],
    agendaData: [],
    galleryData:[]
}

const innovation  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case GETINNOVATIONDATA:   
            return {
                ...state,
                data: action.data
            }
        case GET_CONTACT_USER_DATA:
            return {
                ...state,
                contactPerson: action.data
            }
        case GET_CONCLAVE_DATA:
            return {
                ...state,
                conclave: action.data
            }
        case VIEW_INNOVATION_IMAGE:
            return {
                ...state,
                imageData: action.data
            }
        case GETINNOVATIONLOCATION:
            return {
                ...state,
                hotelLocation: action.data
            }
        case VIEW_INNOVATION_USERS:
            return {
                ...state,
                innovationUsers: action.data
            }
        case VIEW_INNOVATION_SPEARK:
            return {
                ...state,
                speake: action.data
            }
        case VIEW_INNOVATION_HELPDESK:
            return {
                ...state,
                helpDesk: action.data
            } 
            
        case AGENDA_DETAILS:
            return {
                ...state,
                agendaData: action.data
            }
        case GALLERY_DETAILS:
            return {
                ...state,
                galleryData: action.data
            }
    }
    return state;
}

export default innovation;