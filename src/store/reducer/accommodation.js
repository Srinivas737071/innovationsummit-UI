import { 
    USER_ACCOMMODATION_DETAILS, 
    SEARCH_REGISTATION_USER,
    CLEAR_REGISTRATION_USER } from '../actions/action';

const initialState = {
    AccommodationData: [],
    registrationData: []
}

const Accommodation  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case USER_ACCOMMODATION_DETAILS:   
            return {
                ...state,
                AccommodationData: action.data,
                registrationData: action.data
            }
        case SEARCH_REGISTATION_USER:
            return {
                ...state,
                AccommodationData: action.data
            }
        case CLEAR_REGISTRATION_USER: 
            return {
                ...state,
                AccommodationData: state.registrationData
            }
    }
    return state;
}

export default Accommodation;