import { USER_TRAVEL_DETAILS } from '../actions/action';

const initialState = {
    travelData: []
}

const travel  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case USER_TRAVEL_DETAILS:   
            return {
                ...state,
                travelData: action.data
            }
    }
    return state;
}

export default travel;