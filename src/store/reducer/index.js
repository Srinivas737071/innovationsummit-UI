import { combineReducers } from 'redux';
import reducer from './reducer';
import hotel from './hotel';
import innovation from './innovation';
import travel from './travel';
import itenery from './itenery';
import kiosk from './kiosk';
import accommodation from './accommodation';
import { connectRouter } from 'connected-react-router';
 
const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  reducer,
  hotel,
  innovation,
  travel,
  accommodation,
  itenery,
  kiosk
  // rest of your reducers
});

export default createRootReducer;