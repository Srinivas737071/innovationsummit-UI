import { GETUSERDATA ,SEARCHUSER, CLEARUSER} from '../actions/action';

const initialState = {
    data: [],
    responceData:[]
}

const reducer  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case GETUSERDATA:   
            return {
                ...state,
                data: action.data,
                responceData:action.data
            }
            case SEARCHUSER:   
                return {
                    ...state,
                    data: action.data
                }
            case CLEARUSER:   
                return {
                    ...state,
                    data: state.responceData
                }      
    }
    return state;
}

export default reducer;