import { ITENERY_DETAILS } from '../actions/action';

const initialState = {
    iteneryData: []
}

const itenery  = (state = initialState, action) => {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case ITENERY_DETAILS:   
            return {
                ...state,
                iteneryData: action.data
            }
    }
    return state;
}

export default itenery;