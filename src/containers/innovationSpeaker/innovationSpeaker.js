/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import Innovation_Speaker from '../../components/innovationSpeark/innovationSpeark';
import Footer from '../../components/footer/footer';

class InnovationSpeaker extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <Innovation_Speaker />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default InnovationSpeaker;