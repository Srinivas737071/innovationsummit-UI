import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import InnovationUser from '../../components/innovationUserContent/innovationUserContent';
import Footer from '../../components/footer/footer';

class Hotel extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <InnovationUser />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Hotel;