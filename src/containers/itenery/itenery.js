import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import IteneryContent from '../../components/itenerycontent/itenerycontent';
import Footer from '../../components/footer/footer';

class Itenery extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <IteneryContent></IteneryContent>
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Itenery;