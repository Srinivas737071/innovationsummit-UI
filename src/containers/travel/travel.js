import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import TravelDetails from '../../components/travelTable/travelTable';
import Footer from '../../components/footer/footer';

class Travel extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <TravelDetails />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Travel;