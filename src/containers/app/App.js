/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PrivateRoute from '../../components/protectedRoute/protectedRoute';
import AdminLogin from '../adminLogin/adminLogin';
import Dashboard from '../dashboard/dashboard';
import Innovation from '../innovation/innovation';
import InnovationUser from '../innovationUsers/innovationUsers';
import InnovationSpeaker from '../innovationSpeaker/innovationSpeaker';
import InnovationHelpDeskDeatils from '../innovationHelpDesk/innovationHelpDesk';
import Travel from '../travel/travel';
import Gallery from '../gallery/gallery';
import Itenery from '../itenery/itenery';
import Kiosk from '../kiosk/kiosk';

class App extends Component {
  render() {
    let logindata = localStorage.getItem('loginUser');
    return (
      <div className="App">
        <Route exact path="/" 
          render={props =>
          localStorage.getItem('loginUser') ? (
            <Redirect
              to={{
                pathname: "/user",
                state: { from: props.location }
              }}
            />
          ) : (
            <AdminLogin {...props} />
          )
        }/>
        <PrivateRoute exact path="/user" component={Dashboard}/>
        <PrivateRoute exact path="/innovation" component={Innovation} />
        <PrivateRoute exact path="/innovationUser" component={InnovationUser} />
        <PrivateRoute exact path="/innovationSpeaker" component={InnovationSpeaker} />
        <PrivateRoute exact path="/innovationHelpDesk" component={InnovationHelpDeskDeatils} />
        <PrivateRoute exact path="/innovationTravel" component={Travel} />
        <PrivateRoute exact path="/gallery" component={Gallery} />
        <PrivateRoute exact path="/itenery" component={Itenery} />
        <PrivateRoute exact path="/kiosk" component={Kiosk} />
      </div>
    );
  }
}

export default App;
