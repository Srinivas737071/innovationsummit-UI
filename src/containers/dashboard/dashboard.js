import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import Content from '../../components/content/content';
import Footer from '../../components/footer/footer';
import './dashboard.css';

class Dashboard extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <Content />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Dashboard;