import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import Gallery_Content from '../../components/gallerycontent/gallerycontent';
import Footer from '../../components/footer/footer';

class Gallery extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <Gallery_Content />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Gallery;