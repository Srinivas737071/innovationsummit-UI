import React from 'react';
import './adminLogin.css';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as actionType from '../../store/actions/action';
import * as Yup from 'yup';

const AdminLogin = (props) => (
    <Formik
    initialValues={{ email: "", password: "" }}
    onSubmit={(values, { setSubmitting }) => {
      setTimeout(() => {
        props.onAdminLogin(values);
        setSubmitting(false);
      }, 500);
    }}
    validationSchema={Yup.object().shape({
        email: Yup.string()
          .email()
          .required("Required"),
        password: Yup.string()
          .required("No password provided.")
          .matches(/(?=.*[0-9])/, "Password must contain a number.")
      })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <form onSubmit={handleSubmit} className="loginForm">
            <h2>Admin Login</h2>
            <div>
            <label htmlFor="email">Email</label>
            <input
              name="email"
              type="text"
              placeholder="Enter your email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.email && touched.email && "error"}
            />
            {errors.email && touched.email && (
              <div className="input-feedback">{errors.email}</div>
            )}
            </div>
            <div>
            <label htmlFor="password">Password</label>
            <input
              name="password"
              type="password"
              placeholder="Enter your password"
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.password && touched.password && "error"}
            />
            {errors.password && touched.password && (
              <div className="input-feedback">{errors.password}</div>
            )}
            </div>
            <div>
            <button className="loginBtn" type="submit" disabled={isSubmitting}>
              Login
            </button>
            </div>
          </form>
        );
      }}
    </Formik>
);

const mapDispatchToProps = dispatch => {
    return {
        onAdminLogin: (values) => dispatch(actionType.login(values))
    }
}

export default connect(null,mapDispatchToProps)(AdminLogin);
