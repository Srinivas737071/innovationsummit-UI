import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import InnovationHelpDesk from '../../components/innovationHelpDesk/innovationHelpDesk';
import Footer from '../../components/footer/footer';

class InnovationHelpDeskDeatils extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <InnovationHelpDesk />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default InnovationHelpDeskDeatils;