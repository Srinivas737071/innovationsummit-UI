import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import KioskContent from '../../components/kioskContent/kioskContent';
import Footer from '../../components/footer/footer';

class Kiosk extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <KioskContent />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Kiosk;