import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from '../../components/header/header';
import SideBar from '../../components/sidebar/sidebar';
import InnovationContent from '../../components/innovationContent/innovationContent';
import Footer from '../../components/footer/footer';

class Hotel extends Component {
    render() {
        return (
            <Layout>
                <Header />
                <Layout>
                    <SideBar />
                    <InnovationContent />
                </Layout>
                <Footer />
            </Layout>
        );
    };
};

export default Hotel;