import React, { Component } from "react";
import {
  Col,
  Row,
  Layout,
  Card,
  Select,
  Input,
  Form,
  Icon,
  Collapse,
  Divider,
  Modal
} from "antd";
import { InfinityTable as Table } from "antd-table-infinity";
import * as moment from "moment";
import "moment/locale/pt-br";
import { connect } from "react-redux";
import * as actionType from "../../store/actions/action";
import "./kioskContent.css";

const { Content } = Layout;
const { Panel } = Collapse;
const { Search } = Input;
const { confirm } = Modal;

class KioskContent extends Component {
  state = {
    visible: false,
    imageData: null
  };
  columns = [
    {
      title: "Name",
      dataIndex: "name",
      width: 100
    },
    {
      title: "Location",
      dataIndex: "location",
      width: 70
    },
    {
      title: "Company",
      dataIndex: "company",
      width: 70
    },
    {
      title: "Contact",
      dataIndex: "primaryContactCountryCode",
      render: (text, row) => <a> {text + " " + row.primaryContactNo} </a>,
      width: 120
    }
  ];

  componentDidMount() {
    this.props.togetInnovationData();
    this.props.togetKioskUserList("");
  }

  handleChange = e => {
    this.props.togetKioskUserList(e);
  };

//   handleNotification = () => {
//     let x;
//     if (this.props.kioskUserList.length > 0) {
//       let x = this.props.kioskUserList.filter(o => {
//         return o.content.travelStatus === "scheduled";
//       });
//       x.sort(function(a, b) {
//         var c = new Date(a.content.endDate);
//         var d = new Date(b.content.endDate);
//         return c - d;
//       });
//     }
//     console.log(x, "sorted");
//   };

  handleTravelStatus = () => {
    confirm({
        title: 'Is Passenger Arrived?',
        content: 'Please cancel if not arrived!',
        onOk() {
          console.log('OK');
        },
        onCancel() {
          console.log('Cancel');
        },
      });
  }

  genExtra = data => (
    <span className="textcapitalize">
      {data.travelType}
      <Divider type="vertical" />
      {data.travelStatus}
    </span>
  );

  handleZoomImage = data => {
    this.setState({ visible: true, imageData: data });
  };

  hideModal = () => {
    this.setState({
      visible: false,
      imageData: null
    });
  };

  render() {
    return (
      <Content className="container">
        <Modal
          visible={this.state.visible}
          onOk={this.hideModal}
          onCancel={this.hideModal}
          okText="OK"
          cancelText="CANCEL"
        >
          <img src={this.state.imageData} className="imageModal" />
        </Modal>
        <Row>
          <Col span={12}>
            <Card className="cardContainer">
              <Row>
                <Col span={12} style={{ marginBottom: "12" }}>
                  <Select
                    style={{ width: "200px" }}
                    onChange={this.handleChange}
                    placeholder="Select a Innovation"
                  >
                    {this.props.innovation.map(item => (
                      <Select.Option key={item._id} value={item.slug}>
                        {item.name}
                      </Select.Option>
                    ))}
                  </Select>
                </Col>
                <Col span={12} style={{ marginBottom: 12 }}>
                  <Search
                    placeholder="Search"
                    onSearch={value => console.log(value)}
                    style={{ width: "200px", float: "right" }}
                  />
                </Col>
              </Row>
              <Row>
                <h1>GUEST TRAVEL DETAILS</h1>
                <Collapse accordion className="accordion">
                  {this.props.kioskUserList.map(k => (
                    <Panel
                      header={k.content.headerTitle}
                      extra={this.genExtra(k.content)}
                    >
                      <div>
                        <b>Start Date: </b>
                        {moment(k.content.startDate)
                          .utc()
                          .format("DD-MM-YYYY hh:mm:ss A")}{" "}
                        <b>End Date: </b>{" "}
                        {moment(k.content.endDate)
                          .utc()
                          .format("DD-MM-YYYY hh:mm:ss A")}
                      </div>
                      <div>
                        <b>From: </b> {k.content.from} <b>To: </b>{" "}
                        {k.content.to}
                      </div>
                      {k.content.isConnectingFlight &&
                      k.content.connectingFlight.length > 0 ? (
                        <div>
                          <h3>Connecting Flight Details</h3>
                          <div>
                            <b>From:</b> {k.content.connectingFlight[0].from}-
                            <b>To: </b> {k.content.connectingFlight[0].to}
                          </div>
                          <div>
                            <b>Start Date:</b>
                            {moment(k.content.connectingFlight[0].fromDate)
                              .utc()
                              .format("DD-MM-YYYY hh:mm:ss A")}
                            -<b>End Date:</b>
                            {moment(k.content.connectingFlight[0].toDate)
                              .utc()
                              .format("DD-MM-YYYY hh:mm:ss A")}
                          </div>
                        </div>
                      ) : null}
                      <div>
                        {k.content.images && k.content.images.length > 0
                          ? k.content.images.map(image => (
                              <span>
                                <img
                                  src={image.thumbUrl}
                                  className="ticketImage"
                                  onClick={() =>
                                    this.handleZoomImage(image.thumbUrl)
                                  }
                                />
                              </span>
                            ))
                          : null}
                      </div>
                      <Table
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 2500, y: 650 }}
                        dataSource={k.registration.persons}
                        className="tableview"
                        bordered
                      />
                    </Panel>
                  ))}
                </Collapse>
              </Row>
            </Card>
          </Col>
          <Col span={12}>
            <Card className="cardContainer">
              <h1>ALERTS</h1>
               {this.props.kioskAlertList && this.props.kioskAlertList.length > 0 ? (
                   this.props.kioskAlertList.map(kiosAlert => (
                       <Collapse  className="KioskAlert">
                       <p key={kiosAlert._id} >
                        <span class="blink-image" ></span> &nbsp;
                        <b>{(kiosAlert.content.headerTitle).toUpperCase()}</b> 
                        &nbsp; is arriving at &nbsp;
                         <b>{moment(kiosAlert.content.endDate).utc().format("DD-MM-YYYY hh:mm:ss A")}</b> &nbsp;
                         <b><a onClick={this.handleTravelStatus} style={{float:"right"}}>Arrived</a></b>
                       </p>
                       </Collapse>
                   ))
               ): null}
            </Card>
          </Col>
        </Row>
      </Content>
    );
  }
}

const mapStateToProps = state => {
  return {
    innovation: state.innovation.data,
    kioskUserList: state.kiosk.kioskUserlist,
    kioskAlertList: state.kiosk.kioskAlertlist
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togetInnovationData: () => dispatch(actionType.getInnovationData()),
    togetKioskUserList: slug => dispatch(actionType.kioskList(slug))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KioskContent);
