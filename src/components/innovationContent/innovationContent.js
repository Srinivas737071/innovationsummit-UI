/* eslint-disable no-extra-bind */
/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable no-dupe-class-members */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { Row, Col, Layout, Icon, Divider,Popconfirm, Input ,Collapse } from 'antd';
import  { connect } from 'react-redux';
import AddInnovation from '../addInnovation/addInnovation';
import { InfinityTable as Table } from 'antd-table-infinity';
import * as actionType from '../../store/actions/action';
import ViewInnovation from '../viewInnovationImage/viewInnovationImage';
import AddInnovationImage from '../addImageModal/addImageModal';
import { Link } from 'react-router-dom';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import './innovationContent.css';
const { Content } = Layout;
const { Search } = Input;
const { Panel } = Collapse;
class InnovationContent extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title:'Name',
                dataIndex:'name',
                width:30
            },
            {
              title: 'Email',
              dataIndex: 'email',
              width:30
            },
            {
                title:'Location',
                dataIndex:'location',
                width: 30
            },
            {
                title:'Designation',
                dataIndex:'designation',
                width: 30
            },
            {
                title:'Company',
                dataIndex:'company',
                width:30
            },
            {
                title:'Contact',
                dataIndex: 'primaryContactCountryCode',
                render: (text, row) => <a> {text +' '+row.primaryContactNo} </a>,
                width:30
            },
        ];
        this.state = {
            visible: false,
            showAddImage: false,
            innovationImages: null,
            editable: false,
            showImage: false,
            editInnovationData: [],
            addImageData: []
        };
    }

    componentDidMount() {
        this.props.getUserData();
        this.props.togetUserData();
        this.props.togetInnovationData();
        this.props.togetGalleryDetails();
    }
    genExtra = (innovationData) => (
        <span>
            <a onClick={(event) => {this.handleAddImage(innovationData); event.stopPropagation();}}>Add Image</a>
            <Divider type="vertical" />
            <a onClick={(event) => {this.handleImageView(innovationData); event.stopPropagation();}}>View Image</a>
            <Divider type="vertical" />
            <Link to={`/innovationUser?id=${btoa(innovationData.slug)}`}>Registration</Link>
            <Divider type="vertical" />
            <Link to={`/innovationSpeaker?id=${btoa(innovationData.slug)}`}>Agenda</Link>
            <Divider type="vertical" />
            <a onClick={(event) => {this.handleInnovationEdit(innovationData) ; event.stopPropagation(); }}>Edit</a>
            <Divider type="vertical" />
            {this.props.data.length >= 1 ? (
                <Popconfirm title="Sure to delete?" onConfirm={(event) => {this.props.todeleteUser(innovationData.id); event.stopPropagation();}}>
                <a>Delete</a>
                </Popconfirm>
            ) : null}
        </span>
        
      );
    handleInnovationEdit = (data) => {
        this.setState({visible: true,editInnovationData: data,editable: true});
    } 

    handleAddImage = (data) => {
        this.setState({ showAddImage: true, addImageData: data });
    }

    showModal = () => {
        this.setState({ visible: true });
    };
    
    handleCancel = () => {
        this.setState({ visible: false,  showAddImage: false, editable: false, editInnovationData: []});
    };

    handleAddOk = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
            return;
        }
        if(values['Images'] ){
             let s = values['Images'].map((value) => value.originFileObj);
             let data= new FormData();
                data.append('files', s[0]);
                data.append('field', values['field']);
                data.append('ref', 'conclave');
                data.append('refId', this.state.addImageData._id);
                setTimeout(
                function() {
                    // this.props.onAddHotel(values);
                    this.props.onAddInnovationImage(data);
                }
                .bind(this),
                100
                );
          }
        form.resetFields();
            this.setState({ showAddImage: false });
        });
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    viewImageModal(showImage) {
        this.setState({ showImage });
       // this.props.onViewImage()
    };

    handleImageView = (data) => {
            this.setState({
                showImage: true,
                innovationImages: data
            });
    };

    handleCreate = () => {
        const { form } = this.formRefInnovation.props;
        form.validateFields((err, values) => {
            if (err) {
            return;
        }
        if(this.state.editable) {
            values['contactPersons'] = this.props.userdata.filter(o => values.contactPerson.includes(o.email));
            values['slug'] = values['name'].toLowerCase().replace(/ +/g, "");
            delete values.contactPerson;
            this.props.onUpdateInnovation(values,this.state.editInnovationData.id);
            this.setState({editable: false, editInnovationData: []});
        }
        else {
            values['slug'] = values['name'].toLowerCase().replace(/ +/g, "");
            values['contactPersons'] = this.props.userdata.filter(o => values.contactPerson.includes(o.email));
            delete values.contactPerson;
            this.props.onAddInnovation(values);
            this.setState({editable: false});
        }
        form.resetFields();
            this.setState({ visible: false , editable: false, editInnovationData: []});
        });
    };
    
    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    saveFormRefInnovation = formRefInnovation => {
        this.formRefInnovation = formRefInnovation;
    };
    

    render() {
        return (
            <Content className="innovation container">
                <AddInnovation
                    wrappedComponentRef={this.saveFormRefInnovation}
                    visible={this.state.visible}
                    editable={this.state.editable}
                    data={this.props.contactPerson}
                    galleryDetails={this.props.galleryDetails}
                    locationDetails={this.props.locationDetails}
                    editInnovationData={this.state.editInnovationData}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
                <ViewInnovation
                    visible={this.state.showImage}
                    imageData={this.state.innovationImages}
                    onOk={() => this.viewImageModal(false)}
                    onCancel={() => this.viewImageModal(false)}
                />
                <AddInnovationImage
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.showAddImage}
                    onCancel={this.handleCancel}
                    onOk={this.handleAddOk}
                    addImageData={this.state.addImageData}
                />
                <Row justify="end">
                    <Col>
                        <Search
                            placeholder="Search"
                            allowClear
                            onSearch={value => this.props.onInnovationSearch(value,this.props.data)}
                            style={{ width: 200 }}
                            />
                    </Col>
                    <Col style={{float:"right"}}>
                    <Icon type="plus-circle" theme="filled" className="addUser"  onClick={this.showModal}/>
                        <span style={{fontSize:'20px',lineHeight:'40px',marginLeft: "8px",marginRight:'20px'}}>
                            Add Innovation
                        </span> 
                    </Col>
                </Row>
                <Row>

                <div>

          <Collapse accordion>
            {this.props.data.map(innovationData => (
          <Panel header={  innovationData.type  + ' | '+ innovationData.name } key={innovationData.id} extra={this.genExtra(innovationData)}>
            <div><div>Date : </div>{ 'From '+ moment(innovationData.startDate).format("DD-MM-YYYY") + ' to ' + moment(innovationData.endDate).format("DD-MM-YYYY")  + ' Number of days is ' + (moment(moment(innovationData.endDate).format("DD.MM.YYYY"), "DD.MM.YYYY").diff(moment(moment(innovationData.startDate).format("DD.MM.YYYY"), "DD.MM.YYYY"),'days') + 1 ) + ' days'}</div>   
            <div><div>Description :</div> <span  dangerouslySetInnerHTML={{__html: innovationData.description}}/> </div>            
            <div>
            <div> Location Details:</div>
            <div>{innovationData.location.line1}</div>
            <div>{innovationData.location.line2}, {innovationData.location.city} , {innovationData.location.state} , {innovationData.location.country} , {innovationData.location.zipcode} </div>
            <iframe src={innovationData.location.locationMapUrl} width={200}>
                        <p>Your browser does not support iframes.</p>
            </iframe>
            </div>
            <div> Contact Person Details:</div>
            <Table
                        rowKey='_id'
                        pageSize={100}
                        columns={this.columns}
                        dataSource={innovationData.contactPersons}
                        className="contactPerson"
                    />   
          </Panel>
            ))}
        </Collapse>
      </div>  
        </Row>
            </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
        userdata: state.reducer.data,
        data: state.innovation.data,
        contactPerson: state.innovation.contactPerson,
        locationDetails: state.innovation.hotelLocation,
        galleryDetails: state.innovation.galleryData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUserData: () => dispatch(actionType.getContactPersonData()),
        togetInnovationData: () => dispatch(actionType.getInnovationData()),
        onInnovationSearch: (value,data) => dispatch(actionType.searchUser(value,data)),
        onAddInnovation: (value) => dispatch(actionType.addInnovation(value)),
        onUpdateInnovation: (value,id) => dispatch(actionType.updateInnovation(value,id)),
        onAddInnovationImage: (value) => dispatch(actionType.uploadImage(value,'innovation')),
        togetGalleryDetails: () => dispatch(actionType.getGalleryDetails()),
        todeleteUser: (val) => dispatch(actionType.deleteInnovation(val)),
        togetUserData: () => dispatch(actionType.getUserData())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(InnovationContent);