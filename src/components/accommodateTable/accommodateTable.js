import React, { Component } from 'react';
import { Layout, Row } from 'antd';
import { InfinityTable as Table } from 'antd-table-infinity';
import { connect } from 'react-redux';
import * as actionType from '../../store/actions/action';
import AccommodationModal from '../accommodationModal/accommodationModal';

const {Content} = Layout;

class AccommodateTable extends Component {
    state = {
        visible: false,
        editable: false,
        AccommodateEditData: []
    }
   columns =
   [
    {
        title:'User Id',
        dataIndex:'UserID',
        width: 240
    },
    {
        title:'Check In',
        dataIndex:'CheckInDate',
        width: 100
    },
    {
        title:'Check out',
        dataIndex:'CheckOutDate',
        width: 120
    },
    {
        title:'Innovation place',
        dataIndex:'Innovation Place',
        width: 120
    },
    {
        title:'Share with id',
        dataIndex:'ShareingWithId',
        width: 120
    },
    {
        title: 'Action',
        key: 'action',
        width: 80,
        render: (text,record) => (
          <span>
            <a onClick={() => this.handleTableEdit(record)}>Edit</a>
          </span>
        ),
    },
   ]

   componentDidMount() {
        let url = window.location.href;
        let urlParams = url.split("?id=");
        let urlParams1 = urlParams[1].split("&userID=");
        let innovationId = atob(urlParams1[0]);
        let userID = atob(urlParams1[1]);
        let data = {
            InnovationID : innovationId,
            UserID: userID
        }
       this.props.toGetAccommodationDetails(data)
    }

    handleTableEdit = (data) => {
        this.setState({visible: true,editable:true, AccommodateEditData: data});
    }

    handleAccommodationEditOk = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
        if (err) {
            return;
        }
        this.props.UpdateAccommodationDetails(values);
        form.resetFields();
        this.setState({ visible: false,editable:false });
        });
    }

    handleAccommodationEditCancel = () => {
        this.setState({visible: false,editable:false});
    }

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return(
            <Content className="innovationUser container">
                <AccommodationModal 
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    editable={this.state.editable}
                    accommodation={this.state.AccommodateEditData}
                    data={this.props.data}
                    onOk={this.handleAccommodationEditOk}
                    onCancel={this.handleAccommodationEditCancel}
                />
                
                <h1>Accommodation Details</h1>
                <Row>
                    <Table
                        rowKey='UserID'
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 2500, y: 650 }}
                        dataSource={this.props.AccommodateData}
                    />
                </Row>
            </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.reducer.data,
        AccommodateData: state.accommodation.AccommodationData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        toGetAccommodationDetails: (data) => dispatch(actionType.getAccommodationDetails(data)),
        UpdateAccommodationDetails: (data) => dispatch(actionType.updateAccommodationDetails(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccommodateTable);