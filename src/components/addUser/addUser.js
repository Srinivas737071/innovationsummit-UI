import React, { Component } from "react";
import Editor from "react-quill-antd";
import "react-quill-antd/dist/index.css";
import "./addUser.css";
import { 
    Button, 
    Modal, 
    Form, 
    Input, 
    Row, 
    Col, 
    Upload, 
    Icon,
    AutoComplete,
    Select
    } from 'antd';
    const { Option } = Select;

const AddUser = Form.create({ name: "form_in_modal" })(
  class extends Component {
    normFile = e => {
      if (Array.isArray(e)) {
        return e;
      }
      return e && e.fileList;
    };
    render() {
      const {
        visible,
        onCancel,
        onCreate,
        form,
        editable,
        editUserData,
      } = this.props;
      const { getFieldDecorator } = form;
      
      const prefixSelector = getFieldDecorator("primaryContactCountryCode", {
        initialValue: editUserData.primaryContactCountryCode
      })(
        <Select style={{ width: 85 }} placeholder="+91">
          <Option value="+91">+91</Option>
          <Option value="87">+87</Option>
        </Select>
      );
      return (
        <Modal
          visible={visible}
          title={editable ? "Update User" : "Create User"}
          okText={editable ? "Update" : "Create"}
          onCancel={onCancel}
          onOk={onCreate}
          destroyOnClose={true}
        >
          <Form layout="vertical" autoComplete="off">
            <Form.Item>
              <Row gutter={8}>
                <Col span={24}>
                  {getFieldDecorator("name", {
                    initialValue: editUserData.name,
                    rules: [{ required: true, message: "Please enter name!" }]
                  })(<AutoComplete placeholder="Name" />)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              <Row gutter={8}>
                <Col span={12}>
                  {getFieldDecorator("email", {
                    initialValue: editUserData.email,
                    rules: [
                      { required: true, message: "Please enter valid email!" }
                    ]
                  })(<AutoComplete placeholder="Email" type="email" />)}
                </Col>
                <Col span={12}  hidden={editable}>
                  {getFieldDecorator("password", {
                    initialValue: editUserData.password,
                    rules: [
                      {
                        required: true,
                        message: "Please enter valid password!"
                      }
                    ]
                  })(<AutoComplete placeholder="Password" type="password" />)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              <Row gutter={8}>
                <Col span={12}>
                  {getFieldDecorator("location", {
                    initialValue: editUserData.location,
                    rules: [
                      { required: true, message: "Please enter location!" }
                    ]
                  })(<AutoComplete placeholder="Location" />)}
                </Col>
                <Col span={12}>
                  {getFieldDecorator("designation", {
                    initialValue: editUserData.designation,
                    rules: [
                      { required: true, message: "Please enter designation" }
                    ]
                  })(<AutoComplete placeholder="Designation" />)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              <Row gutter={8}>
                <Col span={24}>
                  {getFieldDecorator("description", {
                    initialValue: editUserData.description
                      ? editUserData.description
                      : "",
                    rules: [
                      { required: true, message: "Please enter description!" }
                    ]
                  })(<Editor placeholder="Description" />)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              <Row gutter={8}>
                <Col span={12}>
                  {getFieldDecorator("company", {
                    initialValue: editUserData.company,
                    rules: [
                      { required: true, message: "Please enter company name!" }
                    ]
                  })(<AutoComplete placeholder="Company" />)}
                </Col>
                <Col span={12}>
                  {getFieldDecorator("primaryContactNo", {
                    initialValue: editUserData.primaryContactNo,
                    rules: [
                      { required: true, message: "Please enter contact number" }
                    ]
                  })(<Input addonBefore={prefixSelector} type="number" placeholder="Mobile"/>)}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              <Row gutter={8}>
                <Col span={24}>
                  {getFieldDecorator("type", {
                    initialValue: editUserData.type,
                    rules: [{ required: true }]
                  })(
                    <Select
                      showSearch
                      placeholder="Type"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.props.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option value="attendee">Attendee</Option>
                      <Option value="speaker">Speaker</Option>
                      <Option value="helpdesk">Help desk</Option>
                      <Option value="admin">Admin</Option>
                    </Select>
                  )}
                </Col>
              </Row>
            </Form.Item>

            <Form.Item>
              {getFieldDecorator("pictures", {
                valuePropName: "fileList",
                getValueFromEvent: this.normFile
              })(
                <Upload name="logo" action="/upload.do" listType="picture">
                  <Button>
                    <Icon type="upload" /> Click to upload
                  </Button>
                </Upload>
              )}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  }
);

export default AddUser;
