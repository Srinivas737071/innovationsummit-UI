/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import {
    Form, 
    Input, 
    Row, 
    Col, 
    Popconfirm,
    Layout,
    Divider,
    Select,
    Upload,
    Button,
    Icon
    } from 'antd';
import { InfinityTable as Table } from 'antd-table-infinity';
import * as moment from 'moment';
import { connect } from 'react-redux';
import * as actionType from '../../store/actions/action';

const {Content} = Layout;
const { Option } = Select;

const TravelTable = Form.create({ name: 'form_in_modal' })(
class extends Component {
    state = {
        editTravelData: {},
        editable: false,
    }

   columns =
   [{
        title:'User Id',
        dataIndex:'UserID',
        width: 240
    },
    {
        title:'Travel From',
        dataIndex:'TravelingFrom',
        width: 120
    },
    {
        title:'Travel To',
        dataIndex:'TravelingTo',
        width: 120
    },
    {
        title:'Travel Date',
        dataIndex:'TravelingFromDate',
        render:(dataIndex) => <span>{moment(dataIndex).utc().format("DD-MM-YYYY hh:mm:ss A")}</span>,
        width:120
    },
    {
        title:'Return Date',
        dataIndex:'TravelingToDatee',
        render:(dataIndex) => <span>{moment(dataIndex).utc().format("DD-MM-YYYY hh:mm:ss A")}</span>,
        width: 120
    },
    {
        title:'Travel Type',
        dataIndex:'TravelType',
        width: 120
    },
    {
        title:'Travel Details',
        dataIndex:'TravelDetails',
        width: 120
    },
    {
        title:'Travel Ticket',
        dataIndex:'Image',
        render:  (dataIndex) => <img src={dataIndex} height={55} width={60}/>,
        width: 120
    },
    {
        title: 'Action',
        key: 'action',
        width: 80,
        render: (text,record) => (
          <span>
            <a onClick={() => this.handleTravelEdit(record)}>Edit</a>
            <Divider type="vertical" />
            {this.props.data.length >= 1 ? (
                <Popconfirm title="Sure to delete?" onConfirm={() => this.props.deleteTravelDetails({Id: record.Id})}>
                    <a>Delete</a>
                </Popconfirm>
            ) : null}
          </span>
        ),
    },
   ]

   componentDidMount() {
        let url = window.location.href;
        let urlParams = url.split("?id=");
        let data = atob(urlParams[1]);
        this.props.togetUserData();
        this.props.toGetTravelDetails(data);
        this.props.togetInnovationDetails(data);
    }

    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };

    handleTravelEdit = (data) => {
        this.setState({editTravelData: data, editable: true});
    }

    handleSubmit = e => {
        e.preventDefault();
        let url = window.location.href;
        let urlParams = url.split("?id=");
        let slugid = atob(urlParams[1]);
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            let image = [];
            if(values['TravelImage'] ){
                values['TravelImage'].map((value) => {
                this.getBase64(value.originFileObj, (result) => {
                    image.push(result);
                });
                });

            }
                values['Image'] = image;
                values['EventId'] = slugid; 
                setTimeout(
                function() {
                    if(this.state.editable) {
                        values['Id'] = this.state.editTravelData.Id;
                        values['EventObject'] = this.state.editTravelData.EventObject;
                        values['UserObject'] = this.state.editTravelData.UserObject;                     
                        this.props.UpdateTravelDetails(values);
                        this.setState({editTravelData: {},editable: false});
                    }
                    else {
                        values['EventObject'] = this.props.innovationConclave[0];
                        values['UserObject'] = this.props.data.filter(o => values['UserID'].includes(o.email));
                        this.props.toAddTravelDetails(values);
                        this.props.form.resetFields();
                    }
                    
                }
                .bind(this),
                100
            );
            this.props.form.resetFields();
          }
        });
      };

      getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    handleTravelReset = () => {
        this.setState({editTravelData: {},editable: false});
    }

    render() {
        const { data, form } = this.props;
        const { editTravelData } = this.state;
        const { getFieldDecorator } = form;
        return(
            <Content className="innovationUser container">
                <h1>Add User Travel Details</h1>
                <Form layout="vertical" autoComplete='off' onSubmit={this.handleSubmit}>
                    <Form.Item >
                        <Row gutter={8}>
                            <Col span={12}>
                            {getFieldDecorator('UserID', {
                            initialValue: editTravelData.UserID,
                            rules: [{ required: true, message: 'Please select user id!' }],
                            })(
                                <Select placeholder="User ID">
                                    {data.map(d =>{ 
                                        return <Option key={d.email} value={d.email}>{d.name+'-'+d.email}</Option>})}
                                </Select>,
                            )}
                            </Col>
                            <Col span={12}>
                            {getFieldDecorator('TravelType', {
                            initialValue: editTravelData.TravelType,
                            rules: [{ required: true, message: 'Please select travel type!' }],
                            })(
                                <Select placeholder="Travel type">
                                    <Option key="1" value="flight">Flight</Option>
                                    <Option key="2" value="taxi">Taxi</Option>
                                </Select>,
                            )}
                            </Col>
                        </Row>
                    </Form.Item>

                    <Form.Item >
                        <Row gutter={12}>
                            <Col span={12}>
                            {getFieldDecorator('TravelingFrom', {
                            initialValue: editTravelData.TravelingFrom,
                            rules: [{ required: true, message: 'Please enter travel details!' }],
                            })(
                                <Input placeholder="Travelling From" />
                            )}
                            </Col>
                            <Col span={12}>
                            {getFieldDecorator('TravelingTo', {
                            initialValue: editTravelData.TravelingTo,
                            rules: [{ required: true, message: 'Please enter travel details!' }],
                            })(
                                <Input placeholder="Travelling To" />
                            )}
                            </Col>
                        </Row>
                    </Form.Item>

                    <Form.Item >
                        <Row gutter={12}>
                            <Col span={12}>
                            Travel Date:
                            {getFieldDecorator('TravelingFromDate', {
                            initialValue: editTravelData.TravelingFromDate ? moment(editTravelData.TravelingFromDate).utc().format("YYYY-MM-DDTHH:mm:ss") : null,
                            rules: [{ required: true, message: 'Please select travel date!' }],
                            })(
                                <Input placeholder="Travel Date"  type="datetime-local" step="1"/>
                            )}
                            </Col>
                            <Col span={12}>
                            Return Date:
                            {getFieldDecorator('TravelingToDate', {
                            initialValue: editTravelData.TravelingToDate ? moment(editTravelData.TravelingToDate).utc().format("YYYY-MM-DDTHH:mm:ss") : null,
                            rules: [{ required: true, message: 'Please select return date!' }],
                            })(
                                <Input placeholder="Return Date"  type="datetime-local" step="1"/>
                            )}
                            </Col>
                        </Row>
                    </Form.Item>

                    <Form.Item >
                        <Row gutter={8}>
                            <Col span={24}>
                            {getFieldDecorator('TravelDetails', {
                            initialValue: editTravelData.TravelDetails
                            })(
                                <Input type="textarea" placeholder="Travel details" /> 
                            )}
                            </Col>
                        </Row>
                    </Form.Item>

                    <Form.Item>
                        {getFieldDecorator('TravelImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                        })(
                        <Upload name="logo" action="/upload.do" listType="picture">
                        <Button>
                            <Icon type="upload" /> Upload Travel ticket
                        </Button>
                        </Upload>,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                                <Button type="Primary" style={{float:'right'}} htmlType="submit">Submit</Button>
                            </Col>
                            <Col span={12}>
                                <Button type="Primary" style={{float:'left'}} onClick={this.handleTravelReset}>Cancel</Button>
                            </Col>
                            
                        </Row>
                    </Form.Item>
                </Form>
                <h1>Travel Details</h1>
                <Row>
                    <Table
                        rowKey='UserID'
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 2500, y: 650 }}
                        dataSource={this.props.travelData}
                    />
                </Row>
            </Content>
        );
    }
}
);

const mapStateToProps = state => {
    return {
        data: state.reducer.data,
        travelData: state.travel.travelData,
        innovationConclave: state.innovation.conclave
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togetUserData: () => dispatch(actionType.getUserData()),
        togetInnovationDetails : (val) => dispatch(actionType.getInnovationDetails(val)),
        toGetTravelDetails: (data) => dispatch(actionType.getTravelDetails(data)),
        toAddTravelDetails: (data) => dispatch(actionType.addTravelDetails(data)),
        UpdateTravelDetails: (data) => dispatch(actionType.updateTravelDetails(data)),
        deleteTravelDetails: (id) => dispatch(actionType.deleteTravelDetails(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TravelTable);