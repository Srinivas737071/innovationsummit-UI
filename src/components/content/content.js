/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { Row, Col, Layout, Icon, Divider,Popconfirm, Input, Tooltip } from 'antd';
import  { connect } from 'react-redux';
import ConfigSettings from '../../appConfig';
import { InfinityTable as Table } from 'antd-table-infinity';
import './content.css';
import * as actionType from '../../store/actions/action';
import AddUser from '../../components/addUser/addUser';

const { Content } = Layout;
const { Search } = Input;

class Container extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title:'Profile Pic',
                dataIndex: 'picture',
                render:  (dataIndex) => {
                    if(dataIndex) { 
                        let imageUrl = ConfigSettings.swaggerUrl + dataIndex.url;
                        return <img src={imageUrl} height={55} width={60}/>
                    }
                },
                width: 120,
                fixed: 'left'
            },
            {
                title:'Name',
                dataIndex:'name',
                width:180,
            },
            {
              title: 'Email',
              dataIndex: 'email',
              width:240
            },
            {
                title: 'Type',
                dataIndex:'type',
                width:120
            },
            {
                title:'Location',
                dataIndex:'location',
                width: 120
            },
            {
                title:'Designation',
                dataIndex:'designation',
                width: 120
            },
            {
                title:'Description',
                dataIndex:'description',
                ellipsis: true,
                width: 200,
                render: function(dataIndex) {
                    if(dataIndex){
                     return (
                        <Tooltip 
                            placement="top"
                            title={<span dangerouslySetInnerHTML={{__html: dataIndex}}/>}>
                            <div  dangerouslySetInnerHTML={{__html: dataIndex}}/>
                        </Tooltip>
                     );
                    }
                    else
                    return null
                    }
            },
            {
                title:'Company',
                dataIndex:'company',
                width:120
            },
            {
                title:'Contact',
                dataIndex: 'primaryContactCountryCode',
                render: (text, row) => <a> {text +' '+row.primaryContactNo} </a>,
                width:140
            },
            {
                title: 'Action',
                key: 'action',
                width: 120,
                render: (text, record) => (
                  <span>
                    <a onClick={() => this.handleEditUser(record)}>Edit</a>
                    <Divider type="vertical" />
                    {this.props.data.length >= 1 ? (
                        <Popconfirm title="Sure to delete?" onConfirm={() => this.props.onDeleteUserData(record.id)}>
                            <a>Delete</a>
                        </Popconfirm>
                    ) : null}
                  </span>
                ),
                fixed: 'right'
              },
              
        ];

        this.state = {
            visible: false,
            editable: false,
            editUserData: [],
            editId:null
        };
    }

    componentDidMount() {
        this.props.togetUserData()
    }

    handleEditUser = (data) => {
        this.setState({ 
            visible: true, 
            editable: true, 
            editUserData: data ,
            editId:data.id
        });
    };

    showModal = () => {
        this.setState({ visible: true });
    };
    
    handleCancel = () => {
        this.setState({ 
            visible: false, 
            editUserData: [], 
            editable: false
        });
    };
    
    handleCreate = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
            return;
        }
        if(this.state.editable) {
            this.props.onUpdateUserData(values,this.state.editId);
            this.setState({editable: false});
        }
        else {
            values['slug'] = values['name'].toLowerCase().replace(/ +/g, "");
            this.props.onCreateUserData(values);
            this.setState({editable: false});
        }
        form.resetFields();
            this.setState({ visible: false, editable: false });
        });
    }; 

    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return (
            <Content className="container">
                <AddUser
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    editable={this.state.editable}
                    editUserData={this.state.editUserData}
                />
                <Row justify="end">
                    <Col>
                        <Search
                            placeholder="Search"
                            allowClear
                            onSearch={value => this.props.onUserSearch(value,this.props.data)}
                            style={{ width: 200 }}
                            />
                    </Col>
                    <Col style={{float:"right"}}>
                    <Icon type="plus-circle" theme="filled" className="addUser"  onClick={this.showModal}/>
                        <span onClick={this.showModal} style={{fontSize:'20px',lineHeight:'40px',marginLeft: "8px",marginRight:'20px'}}>
                            Add User
                        </span> 
                    </Col>
                </Row>
                <Row>
                    <Table
                        rowKey='id'
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 500, y: 450 }}
                        dataSource={this.props.data}
                    />      
                </Row>
            </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.reducer.data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togetUserData: () => dispatch(actionType.getUserData()),
        onUserSearch: (value,data) => dispatch(actionType.searchUser(value,data)),
        onCreateUserData: (value) => dispatch(actionType.createUserData(value)),
        onUpdateUserData: (value,editId) => dispatch(actionType.updateUserData(value,editId)),
        onDeleteUserData: (key) => dispatch(actionType.deleteUserData(key))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Container);
