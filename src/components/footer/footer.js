import React from 'react';
import { Layout } from 'antd';
import './footer.css';

const { Footer } = Layout;

const footer = () => (
    <Footer className="footer">©2019 Accion Labs, Inc.</Footer>
);

export default footer;