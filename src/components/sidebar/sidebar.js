/* eslint-disable array-callback-return */
import React, { useState , useEffect} from "react";
import { Layout, Menu, Icon } from "antd";
import "./sidebar.css";
import { NavLink } from "react-router-dom";

const { Sider } = Layout;

const tabs = [
  {
    id: "1",
    icon: "user",
    name: "Users",
    link: "user",
    url: ["/user"]
  },
  {
    id: "2",
    icon: "italic",
    name: "Innovation",
    link: "innovation",
    url: ["/innovation","/innovationUser","/innovationSpeaker","/innovationHelpDesk"]
  },
  {
    id: "3",
    icon: "picture",
    name: "Gallery",
    link: "gallery",
    url: ["/gallery"]
  },
  {
    id: "4",
    icon: "file-pdf",
    name: "Itinerry",
    link: "itenery",
    url: ["/itenery"]
  },
  {
    id: "5",
    icon: "file-pdf",
    name: "Kiosk",
    link: "kiosk",
    url: ["/kiosk"]
  }
];
const filterByValue = (array, string) => {
  return array.filter(o => {
    if (o.url.indexOf(string) > -1) {
      return o;
    }
  });
};

const SideBar = () => {
  const [current, setcurrent] = useState();
  useEffect(() => {
 let filter = filterByValue(tabs, window.location.pathname);    
  if (filter[0]) {
     setcurrent(filter[0].id);
  }
}, []);
  return (
    <Sider className="sideBar">
      <Menu
        theme="dark"
        mode="inline"
        selectedKeys={[current]}
      >
        {tabs.map(tabsData => (
          <Menu.Item key={tabsData.id}>
            <NavLink to={tabsData.link} className="nav-text">
              <Icon type={tabsData.icon} /> {tabsData.name}
            </NavLink>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
  );
};

export default SideBar;
