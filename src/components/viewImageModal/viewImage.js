import React, { Component } from 'react';
import { Modal , Icon} from 'antd';
import ConfigSettings from "../../appConfig";
import './viewImage.css'
import { connect } from 'react-redux';
import * as actionType from '../../store/actions/action';
class View extends Component{
    render() {
        const { visible, onCancel, onOk, form, imageData } = this.props;
        return (
            <Modal
                centered
                visible={visible}
                onOk={onOk}
                onCancel={onCancel}
                >
                {this.props.imageData.media ? this.props.imageData.media.map(image => {
                    return (
                        <span className="imageView" key={image._id}>
                            <img src={ConfigSettings.swaggerUrl + image.url}  height="150" width="150"/>
                            <span className="intoicon">
                                <Icon type="close-square" onClick={() => this.props.handleDeleteImage(image._id)}/>
                            </span>
                        </span>
                        )
                }): null}
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        handleDeleteImage: (id) => dispatch(actionType.deleteImage(id))
    }
}

export default connect(null, mapDispatchToProps)(View);