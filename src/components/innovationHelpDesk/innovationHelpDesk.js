/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { InfinityTable as Table } from 'antd-table-infinity';
import * as actionType from '../../store/actions/action';
import { Row, Layout} from 'antd';

const {Content} = Layout;

class InnovationHelpDesk extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title:'Name',
                dataIndex:'name',
                width:80
            },
            {
              title: 'Email',
              dataIndex: 'email',
              width:120
            },
            {
                title:'Location',
                dataIndex:'location',
                width:120
            },
            {
                title:'Designation',
                dataIndex:'designation',
                width:120
            },
            {
                title:'Contact Number',
                dataIndex:'primaryContactNo',
                render: (text,row) => <a>{row.primaryContactCountryCode+''+text}</a>,
                width:100
            },
        ];
    }
    componentDidMount() {
        this.props.togetUserData();
        let url = window.location.href;
        let urlParams =  url.split("?id=");
        let inovationId = atob(urlParams[1]);
        this.props.togetHelpdeskInnovationDeatils({Id:inovationId});
    }

    render() {
        return (
            <Content className="innovationUser container">
                <Row>
                   <h1>Innovation Help Desk Details</h1>
                   <Table
                        rowKey='id'
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 2500, y: 650 }}
                        dataSource={this.props.helpDesk}
                    />
                </Row>
            </Content>
        );
    };
};

const mapStateToProps = state => {
    return {
        data: state.reducer.data,
        helpDesk: state.innovation.helpDesk
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togetUserData: () => dispatch(actionType.getUserData()),
        togetHelpdeskInnovationDeatils: (data) => dispatch(actionType.getHelpdeskInnovationDeatils(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InnovationHelpDesk);