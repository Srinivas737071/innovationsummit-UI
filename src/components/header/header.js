import React from 'react';
import { Layout, Button } from 'antd';
import { Row, Col } from 'antd';
import { withRouter } from "react-router-dom";
import './header.css';

const { Header } = Layout;

const header = (props) => { 
    const handleLogout = () => {
        localStorage.removeItem('loginUser');
        props.history.push('/');
    }
    return (
        <Header className="header">
            <Row >
                <Col span={6}>
                    <img src="/assets/images/logo.png"
                        alt="ACCION LABS"
                        className="AccionLogo"
                        />
                </Col>
                <Col span={12} style={{textAlign:"center"}}>
                    <span className="headerTitle">Admin Panel</span>
                </Col>
                <Col span={6}>
                    <Button type="link" className="logout" onClick={handleLogout} ghost>Logout</Button>
                </Col>
            </Row>
        </Header>
    )
};

export default withRouter(header);