import React, { Component } from 'react';
import { 
    Button, 
    Modal, 
    Form, 
    Row, 
    Col, 
    Upload, 
    Icon,
    Select 
} from 'antd';

const { Option } = Select;

const AddImageModal = Form.create({ name: 'form_in_modal' })(
    class extends Component {
        normFile = e => {
            if (Array.isArray(e)) {
              return e;
            }
            return e && e.fileList;
        };
        render() {
            const {visible, onOk, onCancel,form, galleryForm} = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    centered
                    title="Add Innovation Image"
                    visible={visible}
                    onOk={onOk}
                    onCancel={onCancel}
                    >
                    <Form layout="vertical">
                        <Form.Item >
                            <Row gutter={8}>
                                <Col span={24} hidden={galleryForm? galleryForm : false}>
                                {getFieldDecorator('field', {
                                rules: [{ required: galleryForm? false : true, message:galleryForm? '': 'Please select type!' }],
                                })(
                                    <Select placeholder="Image type">
                                        <Option value="logo">Logo</Option>
                                        <Option value="banner">Banner</Option>
                                    </Select>
                                )}
                                </Col>
                            </Row>
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('Images', {
                                valuePropName: 'fileList',
                                getValueFromEvent: this.normFile,
                            })(
                                <Upload name="Images" action="/upload.do" listType="picture">
                                <Button>
                                    <Icon type="upload" /> Click to upload
                                </Button>
                                </Upload>,
                            )}
                        </Form.Item>
                </Form>
                </Modal>
            );
        }
    }
);
export default AddImageModal;