import React, { Component } from 'react';
import { 
    Modal, 
    Form, 
    Input, 
    Row, 
    Col, 
    AutoComplete } from 'antd';

const { TextArea } = Input;

const AddGallery = Form.create({ name: 'form_in_modal' })(
class extends Component {
    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };

    render() {
        const { 
            visible, 
            onCancel, 
            onCreate, 
            form, 
            editable, 
            editData,
         } = this.props;
        const { getFieldDecorator } = form;

        return (
        <Modal
          visible={visible}
          title={editable? 'Update Gallery' : 'Create Gallery'}
          okText={editable? 'Update' : 'Create'}
          onCancel={onCancel}
          onOk={onCreate}>
            <Form layout="vertical" autoComplete='off'>
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('title', {
                        initialValue: editData.title,
                        rules:[{ required: true, message: 'Please enter title!' }],
                        })(
                            <AutoComplete placeholder="Title" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('description', {
                        initialValue: editData.description ? editData.description : '',
                        })(
                            <TextArea placeholder="Description" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
            </Form>
        </Modal>
        );
    }
}
);

export default AddGallery;
