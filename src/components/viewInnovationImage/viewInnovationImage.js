/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import { Modal } from 'antd';
import ConfigSettings from '../../appConfig';

class ViewInnovation extends Component {
    render() {
        const { visible, onCancel, onOk, imageData } = this.props;
        return (
            <Modal
                centered
                visible={visible}
                onOk={onOk}
                onCancel={onCancel}
            >               
                <h1>Logo</h1>
                <hr />
                {imageData ? <img src={ConfigSettings.swaggerUrl + imageData.logo.url} height="150" width="150" /> : ''}
                <h1>Banner</h1>
                <hr />
                {imageData ? imageData.banner.map(image => {
                    return (
                        <img src={ConfigSettings.swaggerUrl + image.url} key={image._id} height="150" width="150" />
                    )
                }) : ''}
                <h1>Gallery</h1>
                <hr />
                {imageData ? imageData.galleries.map(gallery => {
                    return (
                        <div key={gallery._id}>
                            <p key={gallery._id}>Title: {gallery.title}</p>
                            {gallery.media.map(media => {
                                return (
                                    <span className="imageView">
                                        <img src={ConfigSettings.swaggerUrl + media.url} key={media._id} height="150" width="150" />
                                    </span>
                                )
                            })}
                        </div>
                    )
                }) : ''}
                <h1>Location</h1>
                <hr />
                {imageData && imageData.location.locationGallery.media ? imageData.location.locationGallery.media.map(a => {
                 return (
                    <span className="imageView">
                    <img src={ConfigSettings.swaggerUrl + a.url} key={a._id} height="150" width="150" />
                    </span>
                    )}
                ): null}
                {/* {imageData && imageData.location && imageData.location.locationGallery && imageData.location.locationGallery.media ? imageData.location.locationGallery.media.map(gallery => {
                    return (
                        <div key={gallery._id}>
                            <p key={gallery._id}>Title: {gallery.title}</p>
                            {gallery.media.map(media => {
                                return (
                                    <span className="imageView">
                                        <img src={ConfigSettings.swaggerUrl + media.url} key={media._id} height="150" width="150" />
                                    </span>
                                )
                            })}
                        </div>
                    )
                }) : ''} */}
            </Modal>
        );
    }
}

export default ViewInnovation;