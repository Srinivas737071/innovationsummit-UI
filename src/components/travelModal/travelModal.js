import React, { Component } from 'react';
import {
    Modal, 
    Form, 
    Input, 
    Row, 
    Col, 
    AutoComplete,
    Select,
    Upload,
    Button,
    Icon
    } from 'antd';
    const { Option } = Select;
const Travel = Form.create({ name: 'form_in_modal' })(
class extends Component {
    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };
    render() {
        const { travelShow, onCancel, onOk, travelData, form, data} = this.props;
        const { getFieldDecorator } = form;
        return (
            <Modal
            visible={travelShow}
            title="Travel Details"
            onCancel={onCancel}
            onOk={onOk}>
            <Form layout="vertical" autoComplete='off'>
                <Form.Item hidden>
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('InnovationID', {
                        initialValue: travelData.InnovationID,
                        })(
                            <AutoComplete placeholder="InnovationId" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item hidden>
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('Id', {
                        initialValue: travelData.Id,
                        })(
                            <AutoComplete placeholder="Id" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('UserID', {
                        initialValue: travelData.UserID,
                        rules: [{ required: true, message: 'Please select user id!' }],
                        })(
                            <Select placeholder="User ID" disabled>
                                {data.map(d =>{ 
                                    return <Option key={d.EmailID+' '+d.EmployeeID} value={d.EmailID}>{d.FirstName+' '+d.LastName+'-'+d.EmployeeID}</Option>})}
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={12}>
                        <Col span={12}>
                        {getFieldDecorator('TravelingFrom', {
                        initialValue: travelData.TravelingFrom,
                        rules: [{ required: true, message: 'Please enter travel details!' }],
                        })(
                            <Input placeholder="Travelling From" />
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('TravelingTo', {
                        initialValue: travelData.TravelingTo,
                        rules: [{ required: true, message: 'Please enter travel details!' }],
                        })(
                            <Input placeholder="Travelling To" />
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={12}>
                        <Col span={12}>
                        {getFieldDecorator('TravelingToDate', {
                        initialValue: travelData.TravelingToDate,
                        rules: [{ required: true, message: 'Please select travel date!' }],
                        })(
                            <Input type="date" />
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('TravelingFromDate', {
                        initialValue: travelData.TravelingFromDate,
                        rules: [{ required: true, message: 'Please select return date!' }],
                        })(
                            <Input type="date" />
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('TravelType', {
                        initialValue: travelData.TravelType,
                        rules: [{ required: true, message: 'Please select travel type!' }],
                        })(
                            <Select placeholder="Travel type">
                                <Option key="1" value="flight">Flight</Option>
                                <Option key="2" value="taxi">Taxi</Option>
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('TravelDetails', {
                        initialValue: travelData.TravelDetails
                        })(
                            <Input type="textarea" placeholder="Travel details" /> 
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item>
                    {getFieldDecorator('TravelImage', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                    })(
                    <Upload name="logo" action="/upload.do" listType="picture">
                    <Button>
                        <Icon type="upload" /> Upload Travel ticket
                    </Button>
                    </Upload>,
                    )}
                </Form.Item>
            </Form>
        </Modal>
        );
    }
}
);

export default Travel;