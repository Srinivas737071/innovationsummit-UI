import React, { Component } from 'react';
import {
    Modal, 
    Form, 
    Input, 
    Row, 
    Col, 
    AutoComplete,
    Select
    } from 'antd';

    const { Option } = Select;

const Accommodation = Form.create({ name: 'form_in_modal' })(
class extends Component {
    state = {
        hotelName: ''
    }
    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };

    componentDidMount() {
        let url = window.location.href;
        let urlParams =  url.split("?id=");
        let urlParams1 =  urlParams[1].split("&hotelId=");
        if(urlParams1[1]){
        this.setState({hotelName: atob(urlParams1[1])});
        }
    }

    render() {
        const { visible, onCancel, onOk, accommodation, form, data,editable} = this.props;
        const { getFieldDecorator } = form;
        return (
        <Modal
          visible={visible}
          title="Accommodation"
          onCancel={onCancel}
          onOk={onOk}>
            <Form layout="vertical" autoComplete='off'>
                <Form.Item hidden>
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('InnovationID', {
                        initialValue: accommodation.InnovationID,
                        })(
                            <AutoComplete placeholder="InnovationId" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('UserID', {
                        initialValue: accommodation.UserID,
                        rules: [{ required: true, message: 'Please select user id!' }],
                        })(
                            <Select placeholder="select user" disabled>
                                {data.map(d =>{ 
                                    return <Option key={d.EmailID+' '+d.EmployeeID} value={d.EmailID}>{d.FirstName+' '+d.LastName+'-'+d.EmployeeID}</Option>})}
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item>
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('InnovationPlace', {
                         initialValue: this.state.hotelName || accommodation.InnovationPlace,
                        rules: [{ required: true, message: 'Please enter Innovation Place!' }],
                        })(
                            <Input type="text"  placeholder="Innovation Place" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={12}>
                        <Col span={12}>
                        {getFieldDecorator('CheckInDate', {
                        initialValue: accommodation.CheckInDate,  
                        rules: [{ required: true, message: 'Please select checkin date!' }],
                        })(
                            <Input type="date" />
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('CheckOutDate', {
                        initialValue: accommodation.CheckOutDate,
                        rules: [{ required: true, message: 'Please select checkout date!' }],
                        })(
                            <Input type="date" />
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('ShareingWithId', {
                        initialValue: accommodation.ShareingWithId,
                        })(
                            <Select placeholder="sharing with id">
                                {data.map(d =>{ 
                                    return <Option key={d.EmailID+' '+d.EmployeeID} value={d.EmailID}>{d.FirstName+' '+d.LastName+'-'+d.EmployeeID}</Option>})}
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
            </Form>
        </Modal>
        );
    }
}
);

export default Accommodation;