import React, { Component } from 'react';
import { 
    Modal, 
    Form, 
    Input, 
    Row, 
    Col, 
    AutoComplete,
    Select
    } from 'antd';
    import Editor from "react-quill-antd";
    import "react-quill-antd/dist/index.css";
    import "./addInnovation.css";
    import * as moment from 'moment';
    import 'moment/locale/pt-br';
    const { Option } = Select;
    const { TextArea } = Input;
const AddInnovation = Form.create({ name: 'form_in_modal' })(
class extends Component {
    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };

    render() {
        const { 
            visible, 
            onCancel, 
            onCreate, 
            form, 
            editable, 
            editInnovationData,
            galleryDetails,
         } = this.props;
        const { getFieldDecorator } = form;
        const { data } = this.props;
        const filteredOptions = data;
        return (
        <Modal
          visible={visible}
          title={editable? 'Update Innovation' : 'Create Innovation'}
          okText={editable? 'Update' : 'Create'}
          onCancel={onCancel}
          destroyOnClose={true}
          onOk={onCreate}>
            <Form layout="vertical" autoComplete='off'>
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('name', {
                        initialValue: editInnovationData.name,
                        rules: [{ required: true, message: 'Please enter innovation name!' }],
                        })(
                            <AutoComplete placeholder="Name" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('description', {
                        initialValue: editInnovationData.description ? editInnovationData.description : '',
                        rules: [{ required: true, message: 'Please enter description!' }],
                        })(
                            <Editor placeholder="Description" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('type', {
                        initialValue: editInnovationData.type,
                        rules: [{ required: true }],
                        })(
                           <Select placeholder="Type">
                               <Option value="summit">Summit</Option>
                           </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('summary', {
                        initialValue: editInnovationData.summary ? editInnovationData.summary : ''
                        })(
                            <TextArea rows={4} placeholder="Welcome Note"/>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('startDate', {
                        initialValue: editInnovationData.startDate ? moment(editInnovationData.startDate).format("YYYY-MM-DD") : null,
                        rules: [{ required: true, message: 'Please select start date!' }],
                        })(
                            <Input type="date"  placeholder="Start Date" />,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('endDate', {
                        initialValue: editInnovationData.endDate ? moment(editInnovationData.endDate).format("YYYY-MM-DD") : null,
                        rules: [{ required: true, message: 'Please select end date!' }],
                        })(
                            <Input type="date"  placeholder="End Date" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('contactPerson', {
                        initialValue: editInnovationData.contactPersons ? editInnovationData.contactPersons.map(item => item.email): [],
                        rules: [{ required: true }],
                        })(
                            <Select
                                mode="multiple"
                                placeholder="Select Contact Person"
                                style={{ width: '100%' }}>
                                {filteredOptions.map(item => (
                                <Select.Option key={item.email} value={item.email}>
                                    {item.name+'-'+item.email}
                                </Select.Option>
                                ))}
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item> 

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('galleries', {
                        initialValue: editInnovationData.galleries ? editInnovationData.galleries.map(item => item._id): [],
                        })(
                            <Select
                                mode="multiple"
                                placeholder="Link to gallery"
                                style={{ width: '100%' }}>
                                {galleryDetails.map(item => (
                                <Select.Option key={item._id} value={item._id}>
                                    {item.title}
                                </Select.Option>
                                ))}
                            </Select>,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('location.locationGallery', {
                        initialValue: editInnovationData.location&&editInnovationData.location.locationGallery ? editInnovationData.location.locationGallery._id: '',
                        })(
                            <Select
                                placeholder="Link to location gallery"
                                style={{ width: '100%' }}>
                                {galleryDetails.map(item => (
                                <Select.Option key={item._id+item.title} value={item._id}>
                                    {item.title}
                                </Select.Option>
                                ))}
                            </Select>,
                        )}
                        </Col>
                    </Row> 
                </Form.Item>
                               
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('location.line1', {
                        initialValue: editInnovationData.location ? editInnovationData.location.line1:'',    
                        rules: [{ required: true, message: 'Please enter Loction Name!' }],
                        })(
                            <Input placeholder="Location Name" />,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('location.line2', {
                        initialValue: editInnovationData.location ? editInnovationData.location.line2:'',    
                        rules: [{ required: true, message: 'Please enter address!' }],
                        })(
                            <Input placeholder="Address" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('location.city', {
                        initialValue: editInnovationData.location ? editInnovationData.location.city:'',    
                        rules: [{ required: true, message: 'Please enter city!' }],
                        })(
                            <Input placeholder="City" />,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('location.state', {
                        initialValue: editInnovationData.location ? editInnovationData.location.state:'',    
                        rules: [{ required: true, message: 'Please enter state!' }],
                        })(
                            <Input placeholder="State" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('location.country', {
                        initialValue: editInnovationData.location ? editInnovationData.location.country:'',    
                        rules: [{ required: true, message: 'Please enter country!' }],
                        })(
                            <Input placeholder="Country" />,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('location.zipcode', {
                        initialValue: editInnovationData.location ? editInnovationData.location.zipcode:'',    
                        rules: [{ required: true, message: 'Please enter zipcode!' }],
                        })(
                            <Input placeholder="zip code" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item >
                    <Row gutter={8}>
                        <Col span={12}>
                        {getFieldDecorator('location.locationLatLong', {
                        initialValue: editInnovationData.location ? editInnovationData.location.locationLatLong:'',    
                        rules: [{ required: true, message: 'Please enter location LatLong!' }],
                        })(
                            <Input placeholder="locationLatLong" />,
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('location.locationMapUrl', {
                        initialValue: editInnovationData.location ? editInnovationData.location.locationMapUrl:'',    
                        rules: [{ required: true, message: 'Please enter locationMapUrl!' }],
                        })(
                            <Input placeholder="locationMapUrl" />,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
            </Form>
        </Modal>
        );
    }
}
);

export default AddInnovation;
