import React, { Component } from 'react';
import { Row, Col, Layout, Icon, Divider,Popconfirm, Input, Tooltip } from 'antd';
import { InfinityTable as Table } from 'antd-table-infinity';
import { connect } from 'react-redux';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import * as actionType from '../../store/actions/action';
import AddGallery from '../../components/addGallery/addGallery';
import ViewImage from '../../components/viewImageModal/viewImage';
import AddGalleryImageModal from '../../components/addImageModal/addImageModal';

const { Content } = Layout;
const { Search } = Input;

class Gallery_Content extends Component {
    columns = [
        {
            title:'Title',
            dataIndex:'title',
            width:200
        },
        {
            title:'Description',
            dataIndex: 'description',
            width:200
        },
        {
            title: 'Images',
            key: 'images',
            width: 120,
            render: (text, record) => (
              <span>
                <a onClick={() => this.addImageToGallery(record)}>Add</a>
                <Divider type="vertical" />
                <a onClick={() => this.viewGalleryImages(record)}>View</a>
              </span>
            ),
        },
        {
            title: 'Action',
            key: 'action',
            width: 120,
            render: (text, record) => (
              <span>
                <a onClick={() => this.handleEditGallery(record)}>Edit</a>
                <Divider type="vertical" />
                {this.props.galleryData.length >= 1 ? (
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.props.deleteGalleryDetails(record._id)}>
                        <a>Delete</a>
                    </Popconfirm>
                ) : null}
              </span>
            ),
        },  
    ];

    state = {
        visible: false,
        viewImage: false,
        editable: false,
        imageDetails: [],
        editData: [],
        showGallery: false,
        galleryForm: false,
        galleryRecord: [],
    }

    addImageToGallery = (data) => {
        this.setState({showGallery: true, galleryForm: true,galleryRecord: data });
    }

    handleCancel = () => {
        this.setState({showGallery: false});
    }

    viewGalleryImages = (data) => {
        this.setState({viewImage: true, imageDetails: data});
    }

    handleCancelView = () => {
        this.setState({viewImage: false});
    }

    handleView = () => {
        this.setState({viewImage: false});
    }

    handleEditGallery = (data) => {
        this.setState({visible: true, editable: true, editData: data})
    }

    handleAddGallery = () => {
        this.setState({visible: true});
    }

    handleGalleryCancel = () => {
        this.setState({visible: false, editable: false, editData: []});
    }

    handleGalleryCreate = () => {
        const { form } = this.formRef1.props;
        form.validateFields((err, values) => {
            if (err) {
            return;
            }
            if(this.state.editable) {
                values['dateTime'] = moment().utc().format("DD-MM-YYYY hh:mm:ss A");
                this.props.editGalleryDetails(this.state.editData._id,values);
                this.setState({ visible: false, editable: false, editData: [] });
            }
            else {
                values['dateTime'] = moment().utc().format("DD-MM-YYYY hh:mm:ss A");
                this.props.addGalleryDetails(values);
                this.setState({ visible: false, editable: false, editData: [] });
            }
        form.resetFields();
        });
    }

    componentDidMount() {
        this.props.getGalleryDetails();
    }

    saveFormRef = formRef => {
        this.formRef1 = formRef;
    };

    handleAddOk = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
            return;
        }
        if(values['Images'] ){
             let s = values['Images'].map((value) => value.originFileObj);
             let data= new FormData();
                data.append('files', s[0]);
                data.append('field', 'media');
                data.append('ref', 'gallery');
                data.append('refId', this.state.galleryRecord._id);
                setTimeout(
                function() {
                    // this.props.onAddHotel(values);
                    this.props.addImageToGallery(data);
                }
                .bind(this),
                100
                );
          }
        form.resetFields();
            this.setState({ showGallery: false });
        });
    }

    saveFormImageRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return (
            <Content className="container">
                <AddGallery
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    editData= {this.state.editData}
                    editable={this.state.editable}
                    onCancel={this.handleGalleryCancel}
                    onCreate={this.handleGalleryCreate}
                />
                <AddGalleryImageModal
                    wrappedComponentRef={this.saveFormImageRef}
                    visible={this.state.showGallery}
                    galleryForm={this.state.galleryForm}
                    onCancel={this.handleCancel}
                    onOk={this.handleAddOk}
                />
                <ViewImage 
                    visible={this.state.viewImage}
                    onCancel={this.handleCancelView}
                    onOk={this.handleView}
                    imageData={this.state.imageDetails}
                />
                <Row justify="end">
                    <Col style={{float:"right"}}>
                        <Icon type="plus-circle" theme="filled" className="addUser" onClick={this.handleAddGallery} />
                        <span style={{fontSize:'20px',lineHeight:'40px',marginLeft: "8px",marginRight:'20px'}} onClick={this.handleAddGallery}>
                            Add Gallery
                        </span> 
                    </Col>
                </Row>
                <Row>
                    <Table
                        rowKey='id'
                        pageSize={100}
                        columns={this.columns}
                        scroll={{ x: 250, y: 150 }}
                        dataSource={this.props.galleryData}
                    />      
                </Row>
            </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
        galleryData: state.innovation.galleryData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getGalleryDetails: () => dispatch(actionType.getGalleryDetails()),
        addGalleryDetails: (data) => dispatch(actionType.addGallery(data)),
        editGalleryDetails: (id,data) => dispatch(actionType.editGallery(id,data)),
        deleteGalleryDetails: (id) => dispatch(actionType.deleteGallery(id)),
        addImageToGallery: (data) => dispatch(actionType.uploadImage(data, 'gallery'))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Gallery_Content);