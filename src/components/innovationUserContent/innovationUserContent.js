/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable array-callback-return */
/* eslint-disable react/style-prop-object */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { push } from "react-router-redux";
import { connect } from 'react-redux';
import { InfinityTable as Table } from 'antd-table-infinity';
import * as actionType from '../../store/actions/action';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import './innovationUserContent.css';
import { Row, Col, Layout, Divider,Input, Button,Modal, Select, Form,AutoComplete,Upload,Icon,Checkbox,Collapse} from 'antd';
const { Option } = Select;
const {Content} = Layout;
const { TextArea, Search } = Input;
const { Panel } = Collapse;

const InnovationUser = Form.create({ name: 'form_in_modal' })(
class extends Component {
    normFile = e => {
        if (Array.isArray(e)) {
          return e;
        }
        return e && e.fileList;
    };
    constructor(props) {
        super(props);
        this.columns1 = [
            {
                title:'Time',
                dataIndex:'agendaTime',
                render: (text, row) => <a> {moment(text, "HH:mm").format("hh:mm A")} </a>,
                width:30
            },
            {
              title: 'Title',
              dataIndex: 'agendaTitle',
              width:30
            },
            {
                title:'Note',
                dataIndex:'agendaNote',
                width: 30
            }
        ];
        this.columns = [
            {
                title:'Name',
                dataIndex:'name',
                width:30
            },
            {
              title: 'Email',
              dataIndex: 'email',
              width:30
            },
            {
                title:'Location',
                dataIndex:'location',
                width: 30
            },
            {
                title:'Designation',
                dataIndex:'designation',
                width: 30
            },
            {
                title:'Company',
                dataIndex:'company',
                width:30
            },
            {
                title:'Contact',
                dataIndex: 'primaryContactCountryCode',
                render: (text, row) => <a> {text +' '+row.primaryContactNo} </a>,
                width:30
            },
        ];
        this.state = {
            selectedItems: [],
            showUpload: false,
            editable: false,
            editId: null,
            searchResult: null,
            selectedType: 'accommodation',
            editAccommodationData: {},
            imageData: null,
            registrationType: null,
            travelType: null,
            itenerykey:[0],
            visible: false,
            itenerycount:0,
            connectingFlightKey:[0],
            connectingFlightcount:0,
            connectingFlightValue:false
        };
    }

    handleZoomImage = (data) => {
        this.setState({visible: true,imageData: data})
    }

    hideModal = () => {
        this.setState({
          visible: false,
          imageData: null
        });
    };
    
    handleChange = selectedItems => {
        this.setState({ selectedItems });
    };

    handleAccommodationEdit = (val) => {
        this.handleTravelReset();
        if(val.title === 'travel') {
            this.setState({
                connectingFlightValue: val.content.isConnectingFlight
           });
        }
        if(val.title === 'travel' && val.content.lookupName === "flight") {
            if(val.content.connectingFlight.length > 0)
            {
                let count = val.content.connectingFlight.length - 1;
                let data = [];
                for (let i = 0; i < val.content.connectingFlight.length; i++) {
                    data.push(i);
                     }  
                    this.setState({
                     connectingFlightcount: count,
                     connectingFlightKey:data
                });
            }
        }
        if(val.title === 'itenery'){
            if(val.content.agenda.length > 0)
            {
                let count = val.content.agenda.length - 1;
                let data = [];
                for (let i = 0; i < val.content.agenda.length; i++) {
                    data.push(i);
                     }  
                    this.setState({
                    itenerycount: count,
                    itenerykey:data
                });
            }
          }
       let emailList = val.registration.persons.map(p => p.email);
        this.setState({
            editAccommodationData: val,
            editable: true,
            selectedItems:emailList,
            editId: val.id
        });
    }

    handleCheckBox = (e) => {
        this.setState({connectingFlightValue: e.target.checked})
    }

    handleAccommodationDelete = (val) => {
        this.handleTravelReset();
        this.props.todeleteregistrations(val.id,val.registration.id,this.state.selectedType);
    }

    componentDidMount() {
        this.props.togetUserData();
        let url = window.location.href;
        let urlParams =  url.split("?id=");
        let inovationId = atob(urlParams[1]);
        this.handleRegistrationSelectType(this.state.selectedType);
        this.props.togetInnovationDetails(inovationId);
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            this.setState({selectedType: values['note']});   
            values['persons'] = this.props.data.filter(o => this.state.selectedItems.includes(o.email));
            let user = localStorage.getItem("loginUser");
            let loginuser = JSON.parse(user);
            values['registeredBy'] = loginuser[0];
            values['conclave'] = this.props.innovationConclave[0];
            delete values.person;
            if(this.state.editable) {
             this.props.toeditregistrations(values,this.state.editAccommodationData.registration.id,this.state.editId,values['note']);
                this.setState({
                    editId: null, 
                    editAccommodationData: [], 
                    selectedItems: [],
                    editable: false
                });
            }
            else {
             this.props.toaddregistrations(values,values['note']);
            }
            this.handleTravelReset();
          }
        });
      };

      handleTravelReset = () => {
        this.props.form.resetFields();
        this.setState({
            editAccommodationData: [], 
            selectedItems: [], 
            editable: false,
            editId: null,
            registrationType :null,
            travelType: null, 
            showUpload: false,
            itenerykey:[0],
            itenerycount:0,
            connectingFlightKey:[0],
            connectingFlightcount:0,
            connectingFlightValue:false
        });
      }

      registrationChange = (e) => {
        this.setState({registrationType: e});
        this.props.form.resetFields();
        this.setState({
            editAccommodationData: [], 
            selectedItems: [], 
            editable: false,
            editId: null,
            travelType: null, 
            showUpload: false,
            itenerykey:[0],
            itenerycount:0,
            connectingFlightKey:[0],
            connectingFlightcount:0,
            connectingFlightValue:false 
        });
      }

      travelTypeChange = (e) => { 
        this.setState({travelType: e});
      }

      addAgendaFrom = (e) => {         
          let data = this.state.itenerykey;
          let itenerycount = 1 + this.state.itenerycount;
          data.push(itenerycount);
          this.setState({itenerykey:data,itenerycount: itenerycount});
      }

    removAgendaFrom = (index) => {         
        let data = this.state.itenerykey;
        let indexValue = data.indexOf(index);
            data.splice(indexValue, 1);
           this.setState({itenerykey:data});
    }
    
    addConnectingFligth = (e) => {         
        let data = this.state.connectingFlightKey;
        let connectingFlightcount = 1 + this.state.connectingFlightcount;
        data.push(connectingFlightcount);
        this.setState({connectingFlightKey:data,connectingFlightcount: connectingFlightcount});
    }

     removeConnectingFligth = (index) => {         
      let data = this.state.connectingFlightKey;
      let indexValue = data.indexOf(index);
      data.splice(indexValue, 1);
      this.setState({connectingFlightKey:data});
    }

    handleRegistrationSearch = (e) => {
        this.setState({searchResult: e});
        if(this.state.selectedType){
            this.props.onRegistrationSearch(e,this.state.selectedType);   
        }
        else if(!this.state.selectedType){          
        this.props.onRegistrationSearch(e,null);
        }
    }

    handleRegistrationSelectType = (e) => {        
        this.setState({selectedType: e});
        if(this.state.searchResult){
            this.props.onRegistrationSearch(this.state.searchResult, e);   
        }
        else if(!this.state.searchResult){          
        this.props.onRegistrationSearch(null, e);
        }
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    genExtra = (messages) => (
        <span className="textcapitalize">
           <a onClick={(event) => {this.handleAccommodationEdit(messages); event.stopPropagation();}}>Edit</a>
            <Divider type="vertical" />
           <a onClick={(event) => {this.handleAccommodationDelete(messages); event.stopPropagation();}}>Delete</a>
        </span>
        
      );
      header = (messages) => (
        <span className="textcapitalize">         
      {/* {messages.title} <Divider type="vertical" /> */}  
        {messages.content.headerTitle} 
        <Divider type="vertical" />
        {messages.content.lookupName === 'aeroplane'? 'Flight' : messages.content.lookupName}
        {
        messages.content.travelType ? 
       <span> <Divider type="vertical"/>
        {messages.content.travelType} </span>
         :null
        }
        </span>      
      );
    render() {
        const { selectedItems, editAccommodationData } = this.state;
        const { data} = this.props;
        const { getFieldDecorator } = this.props.form;
        const filteredOptions = data.filter(o => !selectedItems.includes(o));
        return (
            <Content className="innovationUser container">
                <Modal
                visible={this.state.visible}
                onOk={this.hideModal}
                onCancel={this.hideModal}
                okText="OK"
                cancelText="CANCEL"
                className="imageModalpopup"
                >
                    <img src={this.state.imageData} className="imageModal"/>
                </Modal>
                <h1>
                <Button type="primary" onClick={this.props.gotoInnovation} className="gobackbutton">
                    <Icon type="left" />
                    </Button> Registration
                </h1>
                <Form layout="vertical" autoComplete="off" onSubmit={this.handleSubmit} className="formfiled">
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('note', {
                        initialValue: editAccommodationData.title,
                        rules: [{ required: true, message: 'Please select Registration Type' }],
                        })(
                            <Select placeholder="Registration type" onChange={this.registrationChange}>
                                <Option key="1" value="accommodation">Accommodation</Option>
                                <Option key="2" value="travel">Travel</Option>
                                <Option key="3" value="itenery">Itenery</Option>
                            </Select>,
                        )}
                        </Col>
                    </Row>
                </Form.Item>
                <Form.Item >
                    <Row gutter={8}>
                        <Col span={24}>
                        {getFieldDecorator('headerTitle', {
                            initialValue: editAccommodationData.content && editAccommodationData.content.headerTitle ?editAccommodationData.content.headerTitle : '',
                            rules: [{ required: true, message: 'Please Enter Title' }],
                        })(
                            <AutoComplete placeholder="Title" />
                        )}
                        </Col>
                    </Row>
                </Form.Item>  
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                            {getFieldDecorator("person", {
                                initialValue: selectedItems,
                                rules: [{ required: true, message: "Please enter room number!" }]
                                })
                                (<Select
                                mode="multiple"
                                placeholder="Select Innovation User"
                                setFieldsValue={selectedItems}
                                onChange={this.handleChange}
                                style={{ width: '100%' }}>
                                {filteredOptions.map(item => (
                                <Select.Option key={item.email} value={item.email}>
                                    {item.name+' '+item.email}
                                </Select.Option>
                                ))}
                                </Select>)}                               
                            </Col> 
                            {editAccommodationData.title === 'accommodation' || this.state.registrationType === 'accommodation' ? (              
                            <Col span={12}>
                            {getFieldDecorator("lookupName", {
                            initialValue: editAccommodationData.content && editAccommodationData.content.lookupName ?editAccommodationData.content.lookupName : '',
                            rules: [{ required: true, message: "Please enter Room Number!" }]
                            })(<AutoComplete placeholder="Room Number" />)}
                            </Col>     
                             ) : editAccommodationData.title === 'travel' || this.state.registrationType === 'travel' ? (
                            <Col span={12}>
                                {getFieldDecorator('lookupName', {
                                   initialValue: editAccommodationData.content && editAccommodationData.content.lookupName ? editAccommodationData.content.lookupName : this.state.travelType ,
                                    rules: [{ required: true, message: 'Please select Travel Type' }],
                                    })(
                                        <Select placeholder="Travel Type" 
                                        onChange={this.travelTypeChange}>
                                            <Option key="aeroplane" value="aeroplane">Flight</Option>
                                            <Option key="cab" value="cab">Cab</Option>
                                        </Select>
                                        )}
                             </Col>                
                              ): editAccommodationData.title === 'itenery' || this.state.registrationType === 'itenery' ?(
                                <Col span={12}>
                                {getFieldDecorator("lookupName", {
                                initialValue: editAccommodationData.content && editAccommodationData.content.lookupName ?editAccommodationData.content.lookupName : '',
                                rules: [{ required: true, message: "Please enter day type!" }]
                                })(<AutoComplete placeholder="Day type" />)}
                                </Col>
                              ) :null }
                        </Row>
                    </Form.Item>
                    {editAccommodationData.title === 'travel' || this.state.registrationType === 'travel' ? (
                    <Form.Item >
                    <Row gutter={12}>
                        <Col span={12}>
                        {getFieldDecorator('From', {
                         initialValue: editAccommodationData.content && editAccommodationData.content.from ?editAccommodationData.content.from : '',     
                        rules: [{ required: true, message: 'Please enter travel details!' }],
                        })(
                            <Input placeholder="From" />
                        )}
                        </Col>
                        <Col span={12}>
                        {getFieldDecorator('To', {
                        rules: [{ required: true, message: 'Please enter travel details!' }],
                        initialValue: editAccommodationData.content && editAccommodationData.content.to ?editAccommodationData.content.to : ''
                        })(
                            <Input placeholder="To" />
                        )}
                        </Col>
                    </Row>
                </Form.Item>
                  ) : (
                    null
                  )}
                {editAccommodationData.title !== 'itenery' && this.state.registrationType !== 'itenery' ? (
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                            Start Date : {getFieldDecorator("dateTime", {
                            initialValue: editAccommodationData.content && editAccommodationData.content.startDate ? moment(editAccommodationData.content.startDate).utc().format("YYYY-MM-DDTHH:mm:ss") : null,
                            rules: [{ required: true, message: "Please enter Start Date" }]
                            })(<Input type="datetime-local"  placeholder="Start Date" step="1"/>)}
                            </Col>
                            <Col span={12}>
                            End Date : {getFieldDecorator("checkOutDateTime", {
                            initialValue: editAccommodationData.content && editAccommodationData.content.endDate ? moment(editAccommodationData.content.endDate).utc().format("YYYY-MM-DDTHH:mm:ss") : null,
                            rules: [{ required: true, message: "Please enter End Date!" }]
                            })(<Input type="datetime-local"  placeholder="End Date" step="1"/>)}
                            </Col>
                        </Row>
                    </Form.Item>
                     ) : (
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                            Date : {getFieldDecorator("dateTime", {
                            initialValue: editAccommodationData.content && editAccommodationData.content.dateTime ? moment(editAccommodationData.content.dateTime).utc().format("YYYY-MM-DD") : null,
                            rules: [{ required: true, message: "Please enter  Date" }]
                            })(<Input type="date"  placeholder="Date"/>)}
                            </Col>
                            <Col span={12}>
                            Add more itenery list : <Icon type="plus-circle" style={{cursor: 'pointer'}} onClick = {this.addAgendaFrom}/>
                            </Col>
                        </Row>
                    </Form.Item>                  
                      )}

                    {(((editAccommodationData.title === 'travel') && (editAccommodationData.content && editAccommodationData.content.lookupName === 'aeroplane')) || ((this.state.registrationType === 'travel') && (this.state.travelType === 'aeroplane') ) )  ? (
                    <Form.Item>
                        <Row gutter={8}>
                        <Col span={12}>
                            {getFieldDecorator('travelType', {
                                initialValue: editAccommodationData.content && editAccommodationData.content.travelType ?  editAccommodationData.content.travelType : 'arrival',
                                rules: [{ required: true, message: 'Please enter Travel Type!' }]
                            })(
                            <Select placeholder="Travel type">
                                <Option key="arrival" value="arrival">Arrival</Option>
                                <Option key="departure" value="departure">Departure</Option>
                            </Select>
                            )}
                        </Col>
                        <Col span={12}>
                            {getFieldDecorator('travelStatus', {
                                initialValue: editAccommodationData.content && editAccommodationData.content.travelStatus ?  editAccommodationData.content.travelStatus : 'scheduled',
                                rules: [{ required: true, message: 'Please enter Travel Type!' }]
                            })(
                            <Select placeholder="Travel type">
                                <Option key="scheduled" value="scheduled">Scheduled</Option>
                                <Option key="transit" value="transit">Transit</Option>
                                <Option key="transit-completed" value="transit-completed">Transit Completed</Option>
                                <Option key="arrived" value="arrived">Arrived</Option>
                            </Select>
                            )}
                        </Col>
                        </Row>
                </Form.Item>
                 ) : (
                    null
                  )}

                  {(((editAccommodationData.title === 'travel') && (editAccommodationData.content && editAccommodationData.content.lookupName === 'cab')) || ((this.state.registrationType === 'travel') && (this.state.travelType === 'cab') ) )  ? (
                    <Form.Item>
                        <Row gutter={8}>
                        <Col span={8}>
                            {getFieldDecorator('cabdriverName', {
                                initialValue: editAccommodationData.content && editAccommodationData.content.cabdriverName ?  editAccommodationData.content.cabdriverName : '',
                            })(
                            <Input placeholder="Driver Name" /> 
                            )}
                        </Col>
                        <Col span={8}>
                            {getFieldDecorator('cabNumber', {
                                initialValue: editAccommodationData.content && editAccommodationData.content.cabNumber ?  editAccommodationData.content.cabNumber : '',
                            })(
                                <Input placeholder="Cab Number" /> 
                            )}
                        </Col>
                        <Col span={8}>
                            {getFieldDecorator('cabContactNumber', {
                                initialValue: editAccommodationData.content && editAccommodationData.content.cabContactNumber ?  editAccommodationData.content.cabContactNumber : '',
                            })(
                                <Input placeholder="Contact Number" type="number" />
                            )}
                        </Col>
                        </Row>
                    </Form.Item>
                    ) : (
                    null
                  )}

                  {(((editAccommodationData.title === 'travel') && (editAccommodationData.content && editAccommodationData.content.lookupName === 'aeroplane')) || ((this.state.registrationType === 'travel') && (this.state.travelType === 'aeroplane') ) ) ? (                
                    <Form.Item >
                        <Row>
                    <Col span={12}>    
                    {getFieldDecorator('Image', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                        initialValue: editAccommodationData.content && editAccommodationData.content.images ?  editAccommodationData.content.images : '',
                    })(
                    <Upload name="logo" action="/upload.do" listType="picture">
                    <Button>
                        <Icon type="upload" /> Upload Travel ticket
                    </Button>
                    </Upload>,
                    )}
                    </Col>
                    <Col span={6}>
                    {getFieldDecorator('isConnectingFlight', {
                             valuePropName: 'checked',
                             initialValue: editAccommodationData.content && editAccommodationData.content.isConnectingFlight ?  editAccommodationData.content.isConnectingFlight : this.state.connectingFlightValue,
                            })(<Checkbox onChange={this.handleCheckBox}/>)} 
                  </Col>
                  { ( this.state.connectingFlightValue)? (
                  <Col span={6}>
                    <div>
                         Add connecting flight deatils :  
                          <Icon type="plus-circle" style={{cursor: 'pointer'}} onClick = {this.addConnectingFligth}/>
                     </div>       
                  </Col>
                  )  : null} 
                  </Row>
                 
                  { (this.state.connectingFlightValue)? (                     
                   this.state.connectingFlightKey.map((index) => (        
                  <Row gutter={8} key={index}>
                     <Col span={12}>
                      Connecting Flight From : {getFieldDecorator(`connectingFlight[${index}].from`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.connectingFlight && editAccommodationData.content.connectingFlight[index] && editAccommodationData.content.connectingFlight[index].from  ?  editAccommodationData.content.connectingFlight[index].from : null,    
                            rules: [{ required: true, message: "Please enter from" }]
                            })(<AutoComplete placeholder="From" />)}
                            </Col>
                    <Col span={12}>
                     Connecting Flight To : {getFieldDecorator(`connectingFlight[${index}].to`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.connectingFlight &&editAccommodationData.content.connectingFlight[index] && editAccommodationData.content.connectingFlight[index].to  ?  editAccommodationData.content.connectingFlight[index].to : null,    
                            rules: [{ required: true, message: "Please enter to!" }]
                            })(<AutoComplete placeholder="To" />)}
                    </Col>
                    <Col span={12}>
                      Connecting Flight From Date: {getFieldDecorator(`connectingFlight[${index}].fromDate`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.connectingFlight && editAccommodationData.content.connectingFlight[index] && editAccommodationData.content.connectingFlight[index].fromDate  ?  editAccommodationData.content.connectingFlight[index].fromDate : null,    
                            rules: [{ required: true, message: "Please enter from" }]
                            })(<Input type="datetime-local"  placeholder="From Date" step="1"/>)}
                            </Col>
                    <Col span={12}>
                     Connecting Flight To Date: {getFieldDecorator(`connectingFlight[${index}].toDate`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.connectingFlight && editAccommodationData.content.connectingFlight[index] && editAccommodationData.content.connectingFlight[index].toDate  ?  editAccommodationData.content.connectingFlight[index].toDate : null,    
                            rules: [{ required: true, message: "Please enter to!" }]
                            })(<Input type="datetime-local"  placeholder="To Date" step="1"/>)}
                    </Col>
                    <Col span={12}>
                            Remove connecting flight list :<Icon type="minus-circle" style={{cursor: 'pointer'}} onClick = {() => this.removeConnectingFligth(index)}/>
                    </Col>
                  </Row>
                  ))
                  )  : null} 
                </Form.Item>
                 ) : (
                    null
                  )}
                  {editAccommodationData.title === 'itenery' || this.state.registrationType === 'itenery' ? (
                    
                    this.state.itenerykey.map((index) => (    
                        <Form.Item key={index}>
                        <Row gutter={8}>
                            <Col span={12}>
                            Time : {getFieldDecorator(`agenda[${index}].agendaTime`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.agenda[index] && editAccommodationData.content.agenda[index].agendaTime  ?  editAccommodationData.content.agenda[index].agendaTime : null,    
                            rules: [{ required: true, message: "Please enter Time" }]
                            })(<Input type="time"  placeholder="Time"/>)}
                            </Col>
                            <Col span={12}>
                            Title : {getFieldDecorator(`agenda[${index}].agendaTitle`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.agenda[index] && editAccommodationData.content.agenda[index].agendaTitle  ?  editAccommodationData.content.agenda[index].agendaTitle : null,    
                            rules: [{ required: true, message: "Please enter Title!" }]
                            })(<AutoComplete placeholder="Title" />)}
                            </Col>
                            <Col span={24}>
                             Note : {getFieldDecorator(`agenda[${index}].agendaNote`, {
                            initialValue: editAccommodationData.content && editAccommodationData.content.agenda[index] && editAccommodationData.content.agenda[index].agendaNote  ?  editAccommodationData.content.agenda[index].agendaNote : null,      
                            rules: [{ required: true, message: "Please enter Note!" }]
                            })(<TextArea rows={4} placeholder="Note"/>)}
                            </Col>
                            <Col span={12}>
                             {getFieldDecorator(`agenda[${index}].agendaShowTime`, {
                             valuePropName: 'checked',
                             initialValue: editAccommodationData.content && editAccommodationData.content.agenda[index] && editAccommodationData.content.agenda[index].agendaShowTime  ?  editAccommodationData.content.agenda[index].agendaShowTime : true,     
                            rules: [{ required: true, message: "Please enter Note!" }]
                            })(<Checkbox>Show Time</Checkbox>)}
                            </Col>
                            <Col span={12}>
                            Remove itenery list :<Icon type="minus-circle" style={{cursor: 'pointer'}} onClick = {() => this.removAgendaFrom(index)}/>
                            </Col>
                        </Row>
                    </Form.Item> 
                    ))
                     
                 ) : (
                    null
                  )}
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                                <Button type="Primary" style={{float: 'right'}} htmlType="submit">Submit</Button>
                            </Col>
                            <Col span={12}>
                                <Button type="Primary" style={{alignItems: 'left'}} onClick={this.handleTravelReset}>Cancel</Button>
                            </Col>
                        </Row>
                    </Form.Item>  
                </Form>
 
                <Row gutter={8}>
                        <Col span={12}></Col>
                        <Col span={6}> 
                            <Search
                                placeholder="Search by email"
                                allowClear
                                onSearch={this.handleRegistrationSearch}
                            />
                        </Col>
                        <Col span={6} >
                            <Select 
                                placeholder="Registration type" 
                                style={{width:'290px'}}
                                value={this.state.selectedType}
                                onChange={this.handleRegistrationSelectType}
                                
                                >
                                <Option key="1" value="accommodation">Accommodation</Option>
                                <Option key="2" value="travel">Travel</Option>
                                <Option key="3" value="itenery">Itenery</Option>
                            </Select>
                        </Col>
                </Row>

                <Row>
                 <h1 className="textcapitalize">Registration Details : {this.state.selectedType}</h1>
                   <Collapse accordion>
                   {this.props.messagesData.map(messages => {
                      return (
                        <Panel header={this.header(messages)} key={messages.id} extra={this.genExtra(messages)}>   
                        <div key={messages.id + messages.title}>
                            <span className="textcapitalize">
                                {
                                messages.title === 'itenery' ?
                                <span>
                                Date : {messages.content.dateTime} 
                                </span>
                                :null
                                }
                                {
                                messages.title === 'accommodation' ?
                                <span>
                                Check In Date : {moment(messages.content.startDate).utc().format("DD-MM-YYYY hh:mm:ss A")} - Check Out Date : {messages.content.endDate? moment(messages.content.endDate).utc().format("DD-MM-YYYY hh:mm:ss A"):'- NA'}
                                </span>
                                :null
                                }
                                {
                                    messages.title === 'travel' ?
                                    <div>
                                       Start date: {moment(messages.content.startDate).utc().format("DD-MM-YYYY hh:mm:ss A")} - End date: {moment(messages.content.endDate).utc().format("DD-MM-YYYY hh:mm:ss A")}<br/> From: {messages.content.from} - To: {messages.content.to} 
                                        {
                                           messages.content.lookupName === "cab" ? 
                                           (<div>
                                               Cab details - Driver name: {messages.content.cabdriverName} - Cab number: {messages.content.cabNumber} - Contact number: {messages.content.cabContactNumber}
                                           </div>)
                                           :
                                           (<div>
                                               {messages.content.connectingFlight && messages.content.connectingFlight.length > 0 ?  
                                               <div>
                                                   Connecting Flight Details:
                                                    <div>
                                                        From: {messages.content.connectingFlight[0].from} - 
                                                        To: {messages.content.connectingFlight[0].to}
                                                    </div>
                                                    <div>
                                                        Start Date: {messages.content.connectingFlight[0].fromDate} - End Date: {messages.content.connectingFlight[0].toDate}
                                                    </div>
                                               </div>
                                                :
                                                null
                                                }
                                               <div>Status - {messages.content.travelStatus}</div>
                                               <div>
                                                   {(messages.content.images && messages.content.images.length > 0) ? 
                                                   messages.content.images.map(image => 
                                                      <span><img src={image.thumbUrl} className="ticketImage" onClick={() => this.handleZoomImage(image.thumbUrl)}/></span>
                                                   ) : null}
                                               </div>
                                           </div>)
                                        }
                                    </div>
                                    :null

                                }
                            </span>
                            {
                            messages.title === 'itenery' ?                                
                            <Table
                            rowKey={messages.id + messages._id}
                            pageSize={100}
                            columns={this.columns1}
                            scroll={{ x: 2500, y: 650 }}
                            dataSource={messages.content.agenda}
                            className="tableview"
                            />: null 
                            }
                             <Table
                                rowKey={messages.id + messages.title}
                                pageSize={100}
                                columns={this.columns}
                                scroll={{ x: 2500, y: 650 }}
                                dataSource={messages.registration.persons}
                                className="tableview"/>   
                        </div>
                        </Panel>
                        )
                        })                        
                    }

              </Collapse>
                </Row>
            </Content>
        );
    };
});

const mapStateToProps = state => {
    return {
        data: state.reducer.data,
        messagesData: state.accommodation.AccommodationData,
        innovationUsers: state.innovation.innovationUsers,
        innovationConclave: state.innovation.conclave
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togetUserData: () => dispatch(actionType.getUserData()),
        onRegistrationSearch: (email,title) => dispatch(actionType.searchRegistartion(email,title)),
        togetInnovationDetails : (val) => dispatch(actionType.getInnovationDetails(val)),
        togetregistration: (val) => dispatch(actionType.getregistration(val)),
        toaddregistrations: (val,selectedType) => dispatch(actionType.addregistrations(val,selectedType)),
        todeleteregistrations: (messageid,registrationid,selectedType) => dispatch(actionType.deleteMessage(messageid,registrationid,selectedType)),
        toeditregistrations: (val, registrationid,messageid,selectedType) => dispatch(actionType.editregistrations(val, registrationid,messageid,selectedType)),
        gotoInnovation: () => dispatch(push("/innovation"))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(InnovationUser);