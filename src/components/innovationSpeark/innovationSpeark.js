/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from "react-router-redux";
import { Link } from 'react-router-dom';
import { InfinityTable as Table } from 'antd-table-infinity';
import * as actionType from '../../store/actions/action';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { Row, Col, Layout, Divider, Input, Button, Select, Icon, Form, AutoComplete, Collapse} from 'antd';


const {Content} = Layout;
const { Panel } = Collapse;
const { TextArea } = Input;
const InnovationSpeaker = Form.create({ name: 'form_in_modal' })(
class extends Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title:'Name',
                dataIndex:'name',
                width:80
            },
            {
              title: 'Email',
              dataIndex: 'email',
              width:100
            },
            {
                title:'Location',
                dataIndex:'location',
                width: 80
            },
            {
                title:'Company',
                dataIndex:'company',
                width:80
            },
            {
                title:'Contact',
                dataIndex: 'primaryContactCountryCode',
                render: (text, row) => <a> {text +' '+row.primaryContactNo} </a>,
                width:80
            },
        ];

        this.state = {
            selectedItems: [],
            editable: false,
            editId: null,
            editAgendaData:{},
        };
    }
    
    handleChange = selectedItems => {
        this.setState({ selectedItems });
    };

    handleSpeakerEdit = (val) => {
        let emailList = val.speakers.map(s => s.email);
        this.setState({
            editAgendaData: val,
            editable: true,
            selectedItems:emailList,
            editId: val.id
        });
    }

    handleSpeakerDelete = (val) => {
        this.props.deleteInnovationSpeaker(val.id);
    }

    componentDidMount() {
        this.props.togetUserData();
        let url = window.location.href;
        let urlParams =  url.split("?id=");
        let Id = atob(urlParams[1]);
        this.props.togetAgendaDetails(Id);
        this.props.togetInnovationDetails(Id);
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            values['speakers'] = this.props.data.filter(o => this.state.selectedItems.includes(o.email));
            values['conclave'] = this.props.innovationConclave[0];
            delete values.speaker;
            if(this.state.editable) {
                this.props.updateInnovationSpeaker(values,this.state.editId);
                this.setState({editId: null, editAgendaData: [], selectedItems: []});
            }
            else {
                this.props.addInnovationSpeaker(values);
                this.setState({selectedItems: []});
            }
            this.props.form.resetFields();
          }
        });
      };

      handleAgendaReset = () => {
        this.setState({editId: null, editAgendaData: [], selectedItems: []});
      }

      genExtra = (agenda) => (
        <span className="textcapitalize">
            <a onClick={(event) => {this.handleSpeakerEdit(agenda); event.stopPropagation()}}>Edit</a>
            <Divider type="vertical" />
            <a onClick={(event) =>{ this.handleSpeakerDelete(agenda); event.stopPropagation()}}>Delete</a>
        </span>
      );

    

    render() {
        const { selectedItems, editAgendaData } = this.state;
        const { data} = this.props;
        const { getFieldDecorator } = this.props.form;
        const filteredOptions = data.filter(o => !selectedItems.includes(o));
         return (
            <Content className="innovationUser container">
                <h1><Button type="primary" onClick={this.props.gotoInnovation} className="gobackbutton">
                    <Icon type="left" />
                    </Button> Add Speaker</h1>
                <Form layout="vertical" autoComplete="off" onSubmit={this.handleSubmit} className="formfiled">
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                                {getFieldDecorator("title", {
                                initialValue: editAgendaData.title,
                                rules: [{ required: true, message: "Please enter room number!" }]
                                })(<AutoComplete placeholder="Title" />)}
                            </Col>
                            <Col span={12}>
                            {getFieldDecorator("speaker", {
                                initialValue: selectedItems,
                                })
                                (<Select
                                mode="multiple"
                                placeholder="Select Innovation User"
                                setFieldsValue={selectedItems}
                                onChange={this.handleChange}
                                style={{ width: '100%' }}>
                                {filteredOptions.map(item => (
                                <Select.Option key={item.email} value={item.email}>
                                    {item.name+' '+item.email}
                                </Select.Option>
                                ))}
                                </Select>)}
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item>
                        <Row gutter={8}>
                        <Col span={12}>
                             Start Date : 
                                {getFieldDecorator("startDateTime", {
                                initialValue: editAgendaData.startDateTime ? moment(editAgendaData.startDateTime).utc().format("YYYY-MM-DDTHH:mm:ss") : null,
                                rules: [{ required: true, message: "Please select start date!" }]
                                })(<Input placeholder="Start Date"  type="datetime-local" step="1"/>)}
                            </Col>
                            <Col span={12}>
                            End Date : 
                                {getFieldDecorator("endDateTime", {
                                initialValue: editAgendaData.endDateTime ? moment(editAgendaData.endDateTime).utc().format("YYYY-MM-DDTHH:mm") : null,
                                rules: [{ required: true, message: "Please select end date!" }]
                                })(<Input placeholder="End Date"  type="datetime-local" step="1"/>)}
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={24}>
                                {getFieldDecorator("description", {
                                initialValue: editAgendaData.description,
                                rules: [{ required: true, message: "Please enter description!" }]
                                })(<TextArea rows={4} placeholder="Description" />)}
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item>
                        <Row gutter={8}>
                            <Col span={12}>
                            <Button type="Primary" style={{float: 'right'}} htmlType="submit">Submit</Button>
                            </Col>
                            <Col span={12}>
                            <Button type="Primary" style={{float: 'left'}} onClick={this.handleAgendaReset}>Cancel</Button>
                            </Col>
                            
                        </Row>
                    </Form.Item>
                </Form>
                
                <Row>
                   <h1>Innovation Agenda</h1>
                   <Collapse accordion>
                    {this.props.agendaData.map(agenda => (
                        <Panel header={agenda.title} key={agenda.id + agenda.title} extra={this.genExtra(agenda)} >
                            <div>{agenda.description}</div>
                            <div>
                               <b>Start Date:</b> {moment(agenda.startDateTime).utc().format("DD-MM-YYYY hh:mm:ss A")} 
                               - 
                               <b>End Date:</b> {moment(agenda.endDateTime).utc().format("DD-MM-YYYY, h:mm:ss A") }
                            </div>
                            <h3>Speakers</h3>
                            <Table
                                rowKey={agenda.id}
                                pageSize={100}
                                columns={this.columns}
                                dataSource={agenda.speakers}
                                bordered
                                />
                        </Panel>
                    ))}
                   </Collapse>
                </Row>
            </Content>
        );
    };
});

const mapStateToProps = state => {
    return {
        data: state.reducer.data,
        agendaData: state.innovation.agendaData,
        //accommodationData: state.accommodation.AccommodationData,
        innovationUsers: state.innovation.innovationUsers,
        innovationConclave: state.innovation.conclave
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togetUserData: () => dispatch(actionType.getUserData()),
        togetInnovationDetails : (val) => dispatch(actionType.getInnovationDetails(val)),
        togetAgendaDetails: (id) => dispatch(actionType.getAgendaDetails(id)),
        addInnovationSpeaker: (val) => dispatch(actionType.addAgenda(val)),
        deleteInnovationSpeaker: (id) => dispatch(actionType.deleteAgenda(id)),
        updateInnovationSpeaker: (val, id) => dispatch(actionType.updateAgenda(val, id)),
        gotoInnovation: () => dispatch(push("/innovation"))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(InnovationSpeaker);