/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-useless-constructor */
/* eslint-disable array-callback-return */
import React, { Component } from 'react';
import {Layout, Button, Select, Row, Col} from 'antd';
import * as moment from 'moment';
import 'moment/locale/en-SG';
import { connect } from 'react-redux';
import ConfigSettings from '../../appConfig';
import * as actionType from '../../store/actions/action';
//import { savePDF } from '@progress/kendo-react-pdf';
import { drawDOM, exportPDF } from '@progress/kendo-drawing';

import './itenerycontent.css';
const { Content } = Layout;

class IteneryContent extends Component {
  pdfExportComponent;
   constructor(props) {
      super(props);
      this.state = {
         innovationList: {},
         userList: {}
     };
   }

    componentDidMount() {
       this.props.togetInnovationData();
       this.props.togetUserData();
       
    }

    onChangeInnovation = name => value =>  {
      let data = this.props.innovation.filter(o => value.includes(o._id));
      this.setState({ innovationList: data[0] });
    }

    sentEmail = () => {
      const element = document.getElementById('pdfdoc')
      let config = {
        paperSize: 'A4',
        margin: '0.5cm',
        scale: 0.63,
        keepTogether: '.card',
        };
      let filename = this.state.userList.name + '-itenery-'+ this.state.innovationList.slug + '.pdf';
      drawDOM(element, config).then((group) => {
        return exportPDF(group);
    }).then((dataUri) => {
        let data ={
          pdfbase64 : dataUri,
          filename : filename,
          sentmailto : this.state.userList.email
        }
        this.props.tosentEmailItenery(data)
    });
    }

    onChangeUser = name => (value) => {
      let data = this.props.userData.filter(o => value.includes(o._id));
      this.setState({userList: data[0]});
      this.props.togetItenery(this.state.innovationList.slug,data[0].email);
    }
    render() {
        return (
          <Content className="container">
          <Row >
            <Col span={6}>
            <Select
              style={{ width: 260 }}
              placeholder="Select a Innovation"
              onChange={this.onChangeInnovation('single')}>
               {this.props.innovation.map((item) => (
                  <Select.Option key={item._id} value={item._id}>
                    {item.name}
                   </Select.Option>
               ))}
            </Select>
            </Col>
            <Col span={6}>
            <Select
              style={{ width: 400 }}
              placeholder="Select a user"
              onChange={this.onChangeUser('single')}>
               {this.props.userData.map((item) => (
                  <Select.Option key={item._id} value={item._id}>
                    {item.name+"-"+ item.email}
                   </Select.Option>
               ))}
            </Select>
            </Col>
            </Row>

            <div>
              <br />
              <Button type="primary" onClick={this.sentEmail}>Send email</Button>
            </div>
            <div id="pdfdoc" >
            <div className="wrapper" >
              <div className="left-content">
                 {
                   this.state.innovationList.logo && this.state.innovationList.logo.url ? 
                   (<img
                   src={ ConfigSettings.swaggerUrl + this.state.innovationList.logo.url}
                   className="loc-details-logo logo-url"
                   /> ): ''
                   }
                <div className="clear"> </div>
              <div className="loction">
                <div className="loc-details">
                  <img
                    src="assets/images/location-logo.jpg"
                    className="loc-details-logo"
                  />
                  Location Details
                </div>
                <div className="clear"> </div>
                <h2 className="address">
                   { this.state.innovationList.location && this.state.innovationList.location.line1 ? this.state.innovationList.location.line1 : ''}
                </h2>
                <h3 className="main-address">{this.state.innovationList.location && this.state.innovationList.location.line2 ? this.state.innovationList.location.line2 : ''},{this.state.innovationList.location && this.state.innovationList.location.city ?this.state.innovationList.location.city:''},{this.state.innovationList.location && this.state.innovationList.location.state ?this.state.innovationList.location.state:''},{this.state.innovationList.location && this.state.innovationList.location.country ?this.state.innovationList.location.country:''} {this.state.innovationList.location && this.state.innovationList.location.zipcode ?this.state.innovationList.location.zipcode:''}</h3>                
                </div>
                <div className="thanks-wrapper">
                  Dear <b>{this.state.userList.name},</b> thank you for accepting the invitation to
                  Accion Labs Global Innovation Summit.
                  <br />
                  <br />
                  Please find below the details of your travel, stay and the
                  event schedule:
                </div>
                <div className="clear"> </div>
                <div className="events-wrapper">
                  <div className="fristdiv">
                {this.props.itenery.slice(0, 1).map((iteneryData) => (
                <div className="oneDive">
                  <h6 className="arrival-head">{iteneryData.content && iteneryData.content.lookupName ?iteneryData.content.lookupName : null} </h6>
                   <div className="edit-wrapper">
                    <div className="edit-details">
                      <img src="assets/images/edit.jpg" className="edit-logo" />
                      {iteneryData.content && iteneryData.content.dateTime ? moment(iteneryData.content.dateTime).format("DD MMMM"): null}
                    </div>
                    <div className="clear"> </div>
                    {iteneryData.content && iteneryData.content.agenda ?
                    iteneryData.content.agenda.map((agendaData) => (
                    <div className="time-wrapper">
                   {agendaData.agendaShowTime?
                    <h1 className="live-time">{ moment(agendaData.agendaTime, "HH:mm").format("hh:mm A")}</h1>
                   :<h1 className="live-time">{agendaData.agendaTitle}</h1>}
                   {agendaData.agendaShowTime?
                    <h5 className="sub-title">{agendaData.agendaTitle}</h5>
                    :null}
                      <h6 className="helpdesk">
                        {agendaData.agendaNote}
                      </h6>
                    </div>
                    )):null}                                       
                  </div>   
                  </div>        
                ))}  
              </div>
                  {/*<div className="yoga-wrapper">
                    <img src="assets/images/yoga.jpg" className="yoga-image" />
                    Yoga sessions organised at 7:00 am from 14th to 16th March
                   </div>*/}
                </div>
                <div className="clear"> </div>
                <div className="summit-wrapper">
                  <img src="assets/images/flag.jpg" className="flag" />
                  <h4 className="summit-heading">Need Help? Call us! </h4>
                  <div className="clear"> </div>
                  {this.state.innovationList && this.state.innovationList.contactPersons ? this.state.innovationList.contactPersons.map((item) => (
                    <h5 className="name-summit" key={item._id}>
                    {item.name}
                    <br />
                    {item.email}
                    <br />
                    {item.primaryContactCountryCode} {item.primaryContactNo}
                    <br />                
                  </h5>)): null}
                  <div className="clear"> </div>
                </div>
              </div>
              <div className="right-content">
              <div className="rigth-div">
                { this.props.itenery.slice(1,this.props.itenery.length).map((iteneryData) => (
                  <div className="twodiv">
                <h6 className="arrival-head">{iteneryData.content && iteneryData.content.lookupName ?iteneryData.content.lookupName : null} </h6>         
                <div className="events-wrapper1">                
                  <div className="edit-details">
                    <img src="assets/images/edit.jpg" className="edit-logo" />
                    {iteneryData.content && iteneryData.content.dateTime ? moment(iteneryData.content.dateTime).format("DD MMMM"): null}
                  </div>
                  {iteneryData.content && iteneryData.content.agenda ?
                    iteneryData.content.agenda.map((agendaData) => (
                    <div className="time-wrapper">
                   {agendaData.agendaShowTime?
                    <h1 className="live-time">{ moment(agendaData.agendaTime, "HH:mm").format("hh:mm A")}</h1>
                   :<h1 className="live-time">{agendaData.agendaTitle}</h1>}
                   {agendaData.agendaShowTime?
                    <h5 className="sub-title">{agendaData.agendaTitle}</h5>
                    :null}
                      <h6 className="helpdesk">
                        {agendaData.agendaNote}
                      </h6>
                    </div>
                    )):null} 
                </div>   
                </div>            
               ))}  
               </div>
                <h4 className="wish-heading"> Wish you a pleasant journey </h4>
              </div>
            </div>
            <div className="clear"> </div>
            <div className="footer pdffooter">
              <h1 className="footer-heading">
                We look forward to hosting you.
              </h1>
              <img
                src="assets/images/accion-logo.jpg"
                className="accion-logo"
              />
            </div>
            </div>
          </Content>
        );
    }
}

const mapStateToProps = state => {
    return {
      innovation: state.innovation.data,
      userData: state.reducer.data,
      itenery: state.itenery.iteneryData
    }
}

const mapDispatchToProps = dispatch => {
    return {
      togetInnovationData: () => dispatch(actionType.getInnovationData()),
      togetUserData: () => (dispatch(actionType.getUserData())),
      togetItenery: (id,email) => (dispatch(actionType.getItenery(id,email))),
      tosentEmailItenery: (data) => (dispatch(actionType.sentEmailItenery(data)))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(IteneryContent);