
  export default class ConfigSettings {
    static baseUrl = "http://intranet.accionlabs.com:3000/";
    static graphUrl = "https://event-ms.accionbreeze.com/graphql";
    static swaggerUrl ="https://event-ms.accionbreeze.com/";
    
    static users = "people";
    static innovations = "conclaves";
    static registrations = "registrations";
    static agenda = "sessions";
    static uploadImage = "upload";
    static messages = "messages";
    static deleteImage = "upload/files";
    static galleryDetails = "galleries"
    static getUserTravelDeatils = "user/getUserTravelAllDeatils";
    static addUserTravelDetails = "user/addUserTravelDetails";
    static updateUserTravelDetails = "user/UpdateUserTravelDetails";
    static deleteUserTravelDetails = "user/removeTravel";
    static sentEmailItenery = "user/pdfCreate";
  }
  